# attune-gui Description for Contributors
- Author: Thomas Baldauf
- Contact: thomas.baldauf@dlr.de
- Version: 0.7-maize-alpha

Contributing to attune 
===========================

Project Description
----------------------

Attune is a graphical user interface (GUI) for generating accounting and cash flow patterns for the usage in sfctools. This is the complementary tool to the sfctools framework.  (attune = agent-based transaction gui engine)

Contribute
----------------------

Attune is open for your contribution. If you are interested in contributing to the code or the documentation, you are greatfully invited to do so! If you want to contribute to this repository, please discuss the changes you wish to make, or new features you want to introduce, with the core development team. Please also try to adapt your code style in accordance with the coding conventions and quality values of this repository. Read the Contributors License Agreement (CLA) and send a signed copy to Thomas Baldauf (thomas.baldauf@dlr.de).


More information regarding code structure, design principles and architecture choices can be found in the sfctools documentation. If you wish to contribute, feel free to get in touch directly or open an issue on the repository. We currently don't support automatic pushes. 

The project maintains the following source code repositories

    https://gitlab.com/dlr-ve/esy/sfctools
