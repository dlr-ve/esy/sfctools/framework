# attune - A gui for stock-flow consistent, agent-based modeling

Attune (agent-based transaction and accounting gui) is a graphical user interface (GUI) for generating accounting and cash flow patterns for the usage in sfctools. This is a complementary tool to the sfctools framework.

## Installation 

This is the manual way of obtaining a working copy of the whole repository. This is meant for developers only.
Users should always install the sfctools package via pip. For dependencies, see the pyproject.toml of the project.

1. Get a copy of this repository.
2. Go to this folder. Run the gui via the command

        python attune_starter.py

| Author Thomas Baldauf, German Aerospace Center (DLR), Curiestr. 4 70563 Stuttgart | thomas.baldauf@dlr.de | version: 0.7 (Alpha) | date: February 2022
