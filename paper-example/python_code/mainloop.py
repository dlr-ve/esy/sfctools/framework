from python_code.transactions import *  # import the pre-templated transactions
from python_code.a import A # import the necessary agents
from python_code.b import B
from python_code.c import C

# import sfctools modules 
from sfctools import World, Settings

# import symbolic library 
from sympy import Symbol

# read settings file
Settings().read("settings.yml")

# replace placeholder values with symbolic expressions for 'x' and 'd'
Settings()["d"] = Symbol("d")
Settings()["x"] = Symbol("x")

# create some instances of agents
my_a_agent = A()
my_b_agent = B()
my_c_agent = C()

World().link()

# define one model iteration
def iter():
    my_b_agent.grant_loan()
    my_a_agent.consume()

# define model loop wrapper
def run():
    for i in range(1): # run 1 iteration
        iter()
    
# start model
run()

# import the FlowMatrix from sfctools
from sfctools import FlowMatrix


# print the flow matrix
print(FlowMatrix().to_string())