
from sfctools import World, Agent, Clock, Settings, BalanceEntry, ICSEntry
import numpy as np

from .transactions import *


class A(Agent):

		def __init__(self,*args):
			super().__init__()
		
			with self.balance_sheet.modify:
				self.balance_sheet.change_item("Deposits",BalanceEntry.ASSETS,1000.0,suppress_stock=True)
				self.balance_sheet.change_item("Equity",BalanceEntry.EQUITY,1000.0,suppress_stock=True)
		
			self.C = None


		def link(self):

			self.C = World().get_agents_of_type('C')

			self.x = Settings().get_hyperparameter('x')

		
		def consume(self):
			transaction1(self,self.C[0],self.x)
		
		

