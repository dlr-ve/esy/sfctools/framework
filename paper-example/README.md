# Paper Example

To run the paper example, follow these steps:

 - Read more about the paper example in the paper (JOSS doi xxx-xxx-xxx)

 - Install sfctools (see https://sfctools-framework.readthedocs.io/en/latest/doc_intro/installation.html)

 - Download this folder into your file system

 - In an IDE or Python command prompt of your choice, navigate to the folder containing this file

 - Run 'python run_example.py' to start the example run

 - Your output should be a print of the FlowMatrix from the paper example
