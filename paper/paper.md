---
title: 'sfctools - A toolbox for stock-flow consistent, agent-based models'

tags:
  - Python
  - Agent-Based Modeling
  - Stock-Flow Consistent Modeling
  - User Interface
  - Economics
author:
  - "Thomas Baldauf"
authors:
  - name: Thomas Baldauf
    orcid: 0000-0002-9119-7413
    affiliation: 1
affiliations:
 - name: German Aerospace Center (DLR), Institute of Networked Energy Systems, Curiestr. 4, 70563 Stuttgart, Germany
   index: 1

date: 06 October 2022
bibliography: literature.bib
---

# Summary

One of the most challenging tasks in macroeconomic models is to describe the macro-level effects from the behavior of meso- or micro-level actors. Whereas in 1759, Adam Smith was still making use of the concept of an 'invisible hand' ensuring market stability and economic welfare [@rothschild1994adam], a more and more popular approach is to make the 'invisible' visible and to accurately model each actor individually by defining its behavioral rules and myopic knowledge domain [@castelfranchi2014making;@cincotti2022we]. In agent-based computational economics (ACE), economic actors correspond to dynamically interacting entities, implemented as agents in a computer software [@tesfatsion2002agent;@klein2018agent;@axtell2022agent]. Such agent-based modeling (ABM) is a powerful approach utilized in economic simulations to generate complex dynamics, endogenous business cycles and market disequilibria. For many research topics, it is useful to combine agent-based modeling with the stock-flow consistent (SFC) paradigm [@nikiforos2018stock;@caverzasi2015post;@caiani2016agent]. This architecture ensures there are no 'black holes', i.e. inconsistent sources or sinks, in an economic model. SFC-ABM models, however, are often intransparent and rely on very peculiar, custom-built data structures, thus hampering accessibility [@bandini2009agent;@hansen2019agent]. A tedious task is to generate, maintain and distribute code for ABM, as well as to check for the inner consistency and logic of such models.

# Statement of need

 **sfctools** is an ABM-SFC modeling toolbox, which i) relies on transparent and robust data structures for economic agents, ii) comes along with a simple descriptive modeling approach for agents, iii) provides an easy project builder for Python, making the software runnable and accessible on a large number of platforms, and iv) is also manageable from a graphical user interface (GUI) for ABM-SFC modeling, shipped as part of the toolbox, assuring analytical SFC-check and double accounting consistency. The package is shipped in the form of an open-source project.

**sfctools** was designed to be used by both engineering-oriented and economics-oriented scholars who have basic education in both fields. It can be used by a single developer or by a small development team, splitting the work of model creation in terms of consistency and economic logic from the actual programming and technical implementation. This allows software solutions from rapid prototyping up to more sophisticated, medium-sized ABMs. **sfctools** is therefore a versatile virtual laboratory for agent-based economics.

Unlike more generic frameworks like [*mesa*](https://github.com/projectmesa/mesa/) or [*AgentPy*](https://github.com/joelforamitti/agentpy), it concentrates on agents in economics. Table 1 shows the different feature coverage of selected modeling frameworks and of **sfctools**.[^1] Most other available software packages focus on enabling (multi-) agent-based frameworks at a generic level. However, a standard implementation of accounting balances and the possibility to design and manipulate them in an agent-based environment has not yet been covered. The strength of **sfctools** is therefore three-fold: it tackles the SFC modeling aspect, includes the ABM aspect and provides a graphical interface.

[^1]: A more detailed review about ABM and simulation tools can be found in [@abar2017agent].

Table 1: Comparison of **sfctools** with other modeling frameworks.

| <div style="width:140px">Framework</div>                 | SFC aspect | ABM aspect |  GUI available                 | Language             | Scientific Area            |
|----------------------------------------------------------|------------|------------|--------------------------------|----------------------|----------------------------|
| **sfctools**                                             | x          | x          |  x                             | Python               | Economics                  |
| Mesa [@masad2015mesa]                                    |            | x          | (x)*                          | Python               | Generic                    |
| AgentPy [@Foramitti2021]                                 |            | x          | (x)*                           | Python               | Generic                    |
| ABCEconomics [@abce]                                     |            | x          |  x                             | Python               | Economics                  |
| Foundation [@aieco]                                      |            | x          |                                | Python               | Economics, AI              |
| SFC Models [@sfcmodels]                                  |  x         |            |                                | Python               | Economics                  |
| NetLogo [@g2019netlogo]                                  |            | x          |  x                             | (own)                | Generic                    |
| LSD [@lsd2008]                                           |            | x          |  x                             | C++                  | Economics, Generic         |
| FLAME [@chin2012flame]                                   |            | x          |  x                             | Java                 | Generic                    |
| FAME [@Schimeczek2023]                                         |            | x          |  x                             | Java, Python         | Energy Economics, Generic  |
| JABM  [@jabm]                                            |            | x          |                                | Java                 | Generic                    |
*) via plotting interface

# Basic structure
![Overview of the **sfctools** toolbox. \label{struct}](overview.pdf)

Figure \ref{struct} shows the basic structure of the modeling framework. The framework supports an efficient, yet powerful SFC-ABM model creation and execution workflow. Users can either program their models directly, using the **sfctools** Python package, or can use the graphical user interface (**sfctools-attune**) to design their models at all aggregation levels. This refers to the implementation of behavioral rules and structural parameters (green boxes), and the design of a set of individual balance sheet transactions (plain gray box). Once the basic model setup is created, the users can check for stock-flow consistency by analytically examining the transaction flow matrix (TFM), taking all theoretically allowed transactions and changes in stocks into account. When running the model, the aggregate and disaggregate TFM is available also as a numerical result. The same is true for data structures on the individual agent level (yellow box): the balance sheets, income statements and cashflow statements of individuals can be consistently logged and accessed on runtime or ex-post. In the background, the **sfctools** core framework will take care of all computational operations and thereby assure stock-flow consistency at all times.

![Screenshot of the user interface. 1: Transaction editor, 2: Transactions overview, 3: Graph view. \label{attune}](attune.png)

Figure \ref{attune} shows a screenshot of the user interface **attune** (**a**gen**t**-based **t**ransaction and acco**un**ting graphical user interfac**e**) shipped along with the **sfctools** framework. In this simple graphical interface, transactions can be edited, sorted and graphically analyzed. Also, structural parameters are edited in form of a yaml-styled summary. The GUI allows for several development productivity tools, such as the analytical pre-construction of the TFM. The main window of the GUI consists of three sub-panels: First, it shows the transaction editor panel (1). Here, the user can directly access the balance sheets of the 'sender' and the 'receiver' agent, which are both equipped with a double entry balance sheet system. Also, the entries for income and cash flow can be manually set in this transaction, and the user can define which flows and stocks are addressed. Second, the user can access already created transactions within the transactions overview panel (2). Here, entries can be edited, deleted or sorted, and distinguished features are given as an overview. Third, a graphical representation of the agents is generated in the graph view (3), where different flows and relations are visualized and can be filtered by balance sheet items or by involved agents in an interactive way. The user can freely lay out and colorize the created agent relations in a graph structure.

# Model example

Let us consider a simple model example, namely a three-agent model consisting of a consumer agent (A), a bank (B) and a consumption good producer (C). We employ two transactions (labeled 1 and 2). In the first transaction, B grants a loan to A. Subsequently, A uses its bank deposits to obtain some goods at C. In this simple model, the first transaction only affects the stocks, whereas the second transaction (consumption) is an actual flow.

The model creation workflow is as follows

1. Set up the agents (boxes in the graph view): We add the three agents A, B and C to the model graph. Each node will contain a construction plan for an agent.

2. Set up the transactions (arrows between boxes): Agent A is a consumer and is affected both by both transactions, agent B is a bank and is affected only by transaction 1, agent C is a consumption good producer and is affected only by transaction 2. Once the transactions are registered in the project, they can be deliberately used during the simulation by importing them from an automatically-generated *transactions.py* file.

3. Generate the TFM: To ensure our model is fully stock-flow consistent, we check if all rows and columns of the TFM matrix sum up to zero.

|                       | A    | B  | C  | Total |
|-----------------------|------|----|----|-------|
| Consumption           | -x   | 0  | +x | 0     |
| $\Delta$ Deposits     | -d+x | +d | -x | 0     |
| $\Delta$  Loans       | +d   | -d | 0  | 0     |
| Total                 | 0    | 0  | 0  | 0     |

4. By exporting our model to Python code via saving the project from the GUI, we automatically generate a fully consistent model, usable in any Python script.

Thanks to the user friendliness of **sfctools**, there is little work to be done in terms of coding. In the GUI, we have the possibility to code the three agents in a custom-designed agent parametrization language for **sfctools-attune**. The code is complemented by a simple Python script to finally run the model. The full example can be found on the [project documentation page](https://sfctools-framework.readthedocs.io/en/latest/)[^2] under *Examples*.

[^2]: sfctools-framework.readthedocs.io/en/latest/

# Acknowledgements
I want to thank several people who directly or indirectly got in touch with this software for their constructive remarks: Ardi Latifaj during his master thesis work for his extensive feature requests, Franscesco Lamperti and researchers at Scuola Superiore Sant'Anna for their critical remarks about the framework concept, Patrick Mellacher for his pre-release feedback, Joel Foramitti for his advice on agent-based open-source development, Jonas Eschmann and Luca Fierro for their feedback on the graphical interface and Patrick Jochem for scientific advice on projects being developed using the framework. Karsten Müller and Philipp Harting helped to test the examples. Special thanks go to Benjamin Fuchs for extensive code reviews during the pre-release phase and co-maintenance of the repository. Also, many DLR colleagues supported the project. Last but not least, I am highly grateful to the two reviewers for their excellent constructive feedback on the paper and the code, which has improved the accessibility of the framework for a broad community. All remaining errors are mine.

This work was entirely funded by the German Aerospace Center (DLR).

# References
