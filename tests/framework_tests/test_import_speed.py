import time

def test_import():
    """
    ensures that import of sfctools is fast enough
    """
    start = time.time()
    import sfctools as sfc
    # from sfctools import Agent
    stop = time.time()

    print(stop-start)

    assert stop-start < 1.0 # should take less than a second


if __name__ == "__main__":
    test_import()
