
from sfctools import FlowMatrix,Agent
from sfctools import Accounts
from sfctools import World


def test_world_2():
    """
    This tests wether the World correctly registers agents by their class name
    """

    CA = Accounts.CA
    KA = Accounts.KA

    class MyAgent(Agent):
        def __init__(self):
            super().__init__()

    class NoAgent():
        def __init__(self):
            self.name = "I am a custom agent"

    def my_transaction(a,b,q):
        FlowMatrix().log_flow((CA,CA), q, a, b, subject='Consumption')

    a = MyAgent()
    b = MyAgent()
    c = NoAgent()

    World().register_agent(c,verbose=True)

    assert len(World().agent_types) > 0
    # TODO check dict(World().agent_registry)

    print(World().agent_registry)

    assert len(World().agent_registry["MyAgent"]) == 2

    assert World().agent_registry["MyAgent"][0] == a
    assert World().agent_registry["MyAgent"][1] == b
    assert World().agent_registry["NoAgent"][0] == c

if __name__ == "__main__":
    test_world_2()
