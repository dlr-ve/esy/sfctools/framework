from sfctools.bottomup.matching import MarketMatching
from sfctools import Agent
import numpy as np


def test_market():
    # this tests a simple market example

    class Trader(Agent):
        # a rudimentary trader
        def __init__(self):
            super().__init__()

    supply_traders = [Trader() for i in range(8)]
    demand_traders = [Trader() for i in range(4)] # generate some agents

    class Market(MarketMatching):
        """
        My market for matching supply and demand
        """
        def __init__(self):
            super().__init__()

        def rematch(self):
            # match the suppliers and demanders at random

            np.random.seed(1234) # fix random seed

            for i in self.demand_list:
                for j in self.supply_list:

                    u = np.random.rand()
                    if u > 0.3:
                        self.link_agents(j,i,u)

    # generate a market and add the traders
    my_market = Market()
    [my_market.add_demander(agent) for agent in demand_traders]
    [my_market.add_supplier(agent) for agent in supply_traders]

    # re-match the market traders
    my_market.rematch()

    agents = my_market.get_suppliers_of(demand_traders[0])
    assert(len(agents)> 0)

    print(agents)
    # [<Agent: Trader__00002>, <Agent: Trader__00003>, <Agent: Trader__00004>, <Agent: Trader__00005>, <Agent: Trader__00008>]
    l = [supply_traders[i] for i in [1,2,3,4,7]]
    print(l)

    assert agents == l


if __name__ == "__main__":
    test_market()
