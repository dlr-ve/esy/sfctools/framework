from sfctools import Clock
import datetime
from dateutil.relativedelta import relativedelta
from sfctools.core.singleton import Singleton

def test_clock():
    # a clock with yearly interval
    Clock().let_die()
    
    t00 = datetime.datetime(2020, 1, 1) # set up initial time

    clock = Clock(t0=t00, dt=relativedelta(months=1)) # setup a clock
    assert Clock().t0 == t00 

    t0 = clock.get_time() # get the starting time

    [Clock().tick() for i in range(5)] # tick 5 times

    t1 = clock.get_time()        # get the integer time
    t2 = clock.get_real_time()   # get the actual datetime time

    assert t1 == 5
    assert t2  == datetime.datetime(2020, 6, 1), t2
    
    clock.reset() # reset clock to zero

    t3 = clock.get_time()
    t4 = clock.get_real_time()
    
    # print(clock.dt)
    assert clock.dt == relativedelta(months=1)

    assert t1 == 5
    # assert t2  == datetime.datetime(2025, 1, 1)
    
    assert t0 == 0
    assert t3 == 0
    assert t4 == clock.t0

    assert Clock().t0 == t00
    
    

if __name__ == "__main__":
    test_clock()
