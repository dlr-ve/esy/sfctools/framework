from sfctools import Agent, BalanceEntry, BalanceSheet

def test_balance_sheet_modify():
    """
    tests wether inconsistencies are detected when a balance
    sheet is changed
    """

    # some preparations
    class MyAgent(Agent):
        """
        new dummy agent
        """

        def __init__(self):
            super().__init__()

    a = MyAgent()

    # the actual test
    bal = a.balance_sheet

    # first sub-test: should work
    b_success = False

    try:

        with bal.modify:
            bal.change_item("TestEntry",BalanceEntry.ASSETS,10)
            bal.change_item("TestEntry",BalanceEntry.LIABILITIES,10)

        with bal.modify:
            bal.change_item("TestEntry",BalanceEntry.ASSETS,10)
            bal.change_item("TestEntry",BalanceEntry.EQUITY,9.9999)

        # with bal.modify:
        #   bal.change_item("TestEntry",BalanceEntry.LIABILITIES,0.0)
        # ^supposed to give ' ghost item ' warning

        b_success = True

    except Exception as e:
        print(e)
        b_success = False

    assert b_success, "consistencies confused with inconsistencies"

    # second sub-test: should fail
    a_success = True

    try:
        with bal.modify:
            bal.change_item("TestEntry",BalanceEntry.ASSETS,10)
            bal.change_item("TestEntry",BalanceEntry.LIABILITIES,11)

        with bal.modify:
            bal.change_item("TestEntry",BalanceEntry.ASSETS,10)
            bal.change_item("TestEntry",BalanceEntry.EQUITY,11)

        with bal.modify:
            bal.change_item("TestEntry",BalanceEntry.LIABILITIES,10)

        a_success = True

    except:

        a_success = False

    assert not a_success, "inconsistencies not detected"



def test_balance_sheet_tolerance():
    """
    tests different tolerances for balance sheet consistency
    """

    # some preparations
    class MyAgent(Agent):
        """
        new dummy agent
        """

        def __init__(self):
            super().__init__()

    a = MyAgent()

    # the actual test
    bal = a.balance_sheet

    success_a = True  # sub-test a
    success_b = True  # sub-test b

    # sub-test a
    try:

        with bal.modify:
            bal.change_item("TestEntry",BalanceEntry.ASSETS,10)
            bal.change_item("TestEntry",BalanceEntry.LIABILITIES,11)

    except:

        success_a = False

    # sub-test b
    # use another tolerance level, now it should work
    BalanceSheet.change_tolerance(5.0, 0.01)

    try:

        with bal.modify:
            bal.change_item("TestEntry",BalanceEntry.ASSETS,10)
            bal.change_item("TestEntry",BalanceEntry.LIABILITIES,11)

    except:

        success_b = False


    assert (not success_a) and (success_b), "there is some error in the tolerance level"


def test_balance_flow_matrix_change():
    """
    Tests if flow matrix is affected when balance sheet corrupts
    """

    from sfctools import Agent, FlowMatrix, Accounts, BalanceEntry
    import matplotlib as mpl
    import matplotlib.pyplot as plt

    import numpy as np

    """
    This is a test for the transaction flow matrix. It generates some transactions
    and checks if the matrix is consistently logged.
    """

    FlowMatrix().reset()

    # create some agent classes
    class MyAgent(Agent):
     def __init__(self):
         super().__init__()
         with self.balance_sheet.modify:

            self.balance_sheet.change_item("Cash",BalanceEntry.ASSETS,+100.0,suppress_stock=True)
            self.balance_sheet.change_item("Equity",BalanceEntry.EQUITY,+100.0,suppress_stock=True)

    my_agent = MyAgent()
    print(my_agent.balance_sheet.to_string())
    print(my_agent.balance_sheet.to_dataframe())


    print(FlowMatrix()._flow_data)
    print(FlowMatrix().to_dataframe())


def test_quick_access():
    """
    test __getitem__ magic in agents for accessing the balance sheet
    """

    class MyAgent(Agent):
     def __init__(self):
         super().__init__()
         with self.balance_sheet.modify:

            self.balance_sheet.change_item("Cash",BalanceEntry.ASSETS,+100.0,suppress_stock=True)
            self.balance_sheet.change_item("NetWorth",BalanceEntry.EQUITY,+40.0,suppress_stock=True)
            self.balance_sheet.change_item("Credit",BalanceEntry.LIABILITIES,+60.0,suppress_stock=True)

    a = MyAgent()
    x = a[("Cash", "Assets")]
    y = a[("NetWorth", "Equity")]
    z = a[("Credit", "Liabilities")]

    u = a.balance_sheet.get_balance("Cash", "Assets")
    v = a.balance_sheet.get_balance("NetWorth", "Equity")
    w = a.balance_sheet.get_balance("Credit", "Liabilities")

    assert x == u 
    assert y == v
    assert z == w 


def test_balance_sheet_equity_net_worth():
    """
    Test for the entries EQUITY and NET_WORTH
    """

    # create some agent classes
    class MyAgent(Agent):
     def __init__(self):
         super().__init__()
         with self.balance_sheet.modify:

            self.balance_sheet.change_item("Cash",BalanceEntry.ASSETS,+100.0,suppress_stock=True)
            self.balance_sheet.change_item("NetWorth",BalanceEntry.EQUITY,+100.0,suppress_stock=True)

    # create some agent classes
    class MyAgent2(Agent):
     def __init__(self):
         super().__init__()
         with self.balance_sheet.modify:

            self.balance_sheet.change_item("Cash",BalanceEntry.ASSETS,+100.0,suppress_stock=True)
            self.balance_sheet.change_item("NetWorth",BalanceEntry.NET_WORTH,+100.0,suppress_stock=True)

    a = MyAgent()
    b = MyAgent2()

    u = a[("NetWorth", "Net Worth")]
    v = b[("NetWorth", "Equity")]

    u2 = a[("NetWorth", "Equity")]
    v2 = b[("NetWorth", "Net Worth")]

    y = a[("NetWorth", BalanceEntry.NET_WORTH)]
    z = b[("NetWorth", BalanceEntry.EQUITY)]

    y2 = a[("NetWorth", BalanceEntry.EQUITY)]
    z2 = b[("NetWorth", BalanceEntry.NET_WORTH)]

    l = [u,v,y,z,y2,z2]

    # assert all elements are the same
    assert l.count(l[0]) == len(l)


if __name__ == "__main__":
    
    """
    test_balance_sheet_modify()
    test_balance_sheet_tolerance()
    test_balance_flow_matrix_change()
    """
    
    test_quick_access()
    test_balance_sheet_equity_net_worth()
    