from sfctools import Agent
from sfctools.bottomup.productiontree import ProductionTree


def test_production_tree():
    
    class MyProduction:
        def __init__(self,x):
            self.x = x 
    
    my_agent_a = MyProduction("A")
    my_agent_b = MyProduction("B")
    my_agent_y = MyProduction("Y")
    
    production_dict = {
            "CES_1": {"epsilon": 1.0, "alpha": [0.5,0.3,0.2]}, #...
            }
    
    data =  {"Y;CES_1":
                {"A": {},
                 "B": {}}}

    agent_name_dict = {
                    "A": [my_agent_a],
                    "B": [my_agent_b],
                    "Y": [my_agent_y]
                }
    
    my_production_tree = ProductionTree(production_dict,data,agent_name_dict)
    
    # TODO assertion tests
    # TODO produce some outputs
    
    # print / plot?
    # init nodes 
    # traverse 
    # traverse_nodes 



if __name__ == "__main__":
    
    test_production_tree()


