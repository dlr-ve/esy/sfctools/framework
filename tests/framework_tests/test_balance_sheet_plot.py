

from sfctools import Agent
from sfctools import BalanceSheet
from sfctools import BalanceEntry as BE
import matplotlib.pyplot as plt
import numpy as np

"""
This tests wether the balance sheet plotting passes a basic functionality test
"""

class A(Agent):

    def __init__(self):
        super().__init__()

        with self.balance_sheet.modify:
            self.balance_sheet.change_item("A", BE.ASSETS, 10.0)
            self.balance_sheet.change_item("E", BE.EQUITY, 8.0)
            self.balance_sheet.change_item("L", BE.LIABILITIES, 2.0)


class MyAgent(Agent):
    """
    An example agent
    """
    def __init__(self):
        super().__init__()

        self.change_equity(20.0,"Asset1")
        self.change_equity(10.0,"Asset2")
        self.change_debt(10.0)

    def change_equity(self,q,which): # increase or decrease equity by q / -q
        with self.balance_sheet.modify:
            self.balance_sheet.change_item(which,BE.ASSETS,q)
            self.balance_sheet.change_item("Equity",BE.EQUITY,q)

    def change_debt(self,q): # increase or decrease debt by q / -q
        with self.balance_sheet.modify:
            self.balance_sheet.change_item("Debt",BE.LIABILITIES,q)
            self.balance_sheet.change_item("Asset2",BE.ASSETS,q)

    def swap_assets(self,q): # swap between assets
        try:
            with self.balance_sheet.modify:
                self.balance_sheet.change_item("Asset2",BE.ASSETS,-q)
                self.balance_sheet.change_item("Asset1",BE.ASSETS,q)
        except:
            pass

    def update(self):

        q1 = 0.5*(np.random.uniform(-.1,.5))
        q2 = 0.5*(np.random.uniform(-.1,.5))
        q3 = .8*np.random.uniform(0.1,.5)

        self.change_equity(q1,"Asset1")
        self.change_debt(q1)
        self.swap_assets(q3)

def test_bsplot():
    """
    does the balane sheet plotting function exist?
    """
    success = True
    try:
        a = A()
        # a.balance_sheet.plot(show_labels=True)
        success = hasattr(a.balance_sheet, "plot")
        # plt.close()
    except:
        success = False

    assert success


def test_bsplot2():
    """
    does the balance sheet list plot functionality run?
    """

    success = True
    try:
        my_agent = MyAgent() # create an agent

        my_balances = [] # allocate empty list

        for i in range(80): # iterate simulation
            my_agent.update()
            my_balances.append(my_agent.balance_sheet.raw_data) # append balance sheet data to list


        # plot balance sheet evolution
        BalanceSheet.plot_list(my_balances, dt=5, xlabel="Time", title="My Agent", show_liabilities=True,show=False)
        plt.close()
        success = True

    except Exception as e:
        print(e)
        success = False

    assert success


if __name__ == "__main__":
    test_bsplot()
    test_bsplot2()
