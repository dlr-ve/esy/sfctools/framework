
import time
from sfctools.datastructs.income_statement import ICSEntry,IncomeStatement

from sfctools import Agent, Clock
import numpy as np

"""
this is a test for the income statement.
It will write some elements and see if they can be read out correctly.
"""

def test_ics():
    """
    asserts basic functionality
    """

    a = Agent()
    ics = a.income_statement

    kind = ICSEntry.REVENUES
    tag = "My Subject"
    value = 10.0

    ics.new_entry(kind,tag,value)
    ics.new_entry(ICSEntry.TAXES,"My taxes",-2.0)

    # print(ics.to_string())
    value = ics.get_entry(ICSEntry.REVENUES,"My Subject")
    value2 = ics.get_entry(ICSEntry.TAXES,"My taxes")

    value3 = ics.net_income

    assert value == 10.0
    assert value2 == -2.0
    assert value3== 8.0


def test_ics2():
    """
    asserts functionality of the .last property
    """

    a = Agent()
    ics = a.income_statement

    kind = ICSEntry.REVENUES
    tag = "My Subject"
    value = 10.0

    ics.new_entry(kind,tag,value)
    ics.new_entry(ICSEntry.TAXES,"My taxes",2.0)

    a = ics.to_string()
    ics.reset()

    ics_last = ics.last

    b = ics_last.to_string()

    assert a == b


def test_ics3():
    """
    asserts functionality of the .last property
    """

    a = Agent()
    ics = a.income_statement

    kind = ICSEntry.REVENUES
    tag = "My Subject"
    value = 10.0

    ics.new_entry(kind,tag,value)
    ics.new_entry(ICSEntry.TAXES,"My taxes",2.0)

    a = ics.to_string()
    ics.reset()

    ics_last = ics.last

    b = ics_last.to_string()

    assert a == b

def test_ics4():
    """
    asserts functionality of the .last property
    """

    a = Agent()
    ics = a.income_statement

    kind = ICSEntry.REVENUES
    tag = "My Subject"
    value = 10.0

    for i in range(5):
        # apply random entries
        rand_x = np.random.rand(9)

        #for x in rand_x:
        #    print(x)

        ics.new_entry(ICSEntry.REVENUES,tag,rand_x[0])
        ics.new_entry(ICSEntry.GAINS,tag,rand_x[1])
        ics.new_entry(ICSEntry.EXPENSES,tag,rand_x[2])
        ics.new_entry(ICSEntry.LOSSES,tag,rand_x[3])
        ics.new_entry(ICSEntry.NONTAX_PROFITS,tag,rand_x[4])
        ics.new_entry(ICSEntry.NONTAX_LOSSES,tag,rand_x[5])
        ics.new_entry(ICSEntry.INTEREST,tag,rand_x[6])
        ics.new_entry(ICSEntry.TAXES,tag,rand_x[7])
        ics.new_entry(ICSEntry.NOI,tag,rand_x[8])

        # test if reset functionality works for all entries
        a = ics.to_string()
        # print("OK\n",ics.to_string())

        a_df = ics.to_dataframe()
        ics.reset()

        ics_last = ics.last

        b = ics_last.to_string()
        b_df = ics_last.to_dataframe()
        assert a == b
        assert a_df.equals(b_df)

        # test if the entries are correct after the conversion to a dataframe
        ics_last.net_income
        sum_x_gross = rand_x[0] + rand_x[1] + rand_x[2] + rand_x[3]
        sum_x_net = sum_x_gross +  rand_x[4] + rand_x[5] + rand_x[6] + rand_x[7] + rand_x[8]

        assert abs(ics_last.gross_income - sum_x_gross) < 1e-4
        assert abs(ics_last.net_income - sum_x_net) < 1e-4


def test_ics5():
    """
    asserts functionality of the .get_history funcion
    """

    a = Agent()
    ics = a.income_statement

    for i in range(3):
        ics.new_entry(ICSEntry.REVENUES,"My Subject",10.0)
        ics.new_entry(ICSEntry.TAXES,"My taxes",-2.0)
        ics.new_entry(ICSEntry.LOSSES,"Loss",-2.0)

        ics.reset(keep_last=True)
        Clock().tick()
    
    assert ics.get_history("net_income") == [0.0, 6.0, 6.0, 6.0]
    assert  ics.get_history_entry(ICSEntry.TAXES, "My taxes") == [0.0, -2.0, -2.0, -2.0]
    assert ics.get_history("timestamp") == [2, 2, 1, 0]


if __name__== "__main__":
    
    test_ics()
    test_ics2()
    test_ics3()
    test_ics4()
    test_ics5()
