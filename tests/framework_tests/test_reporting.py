
from sfctools.misc.reporting_sheet import ReportingSheet,IndicatorReport,DistributionReport
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def test_reporting_sheet():
    """
    tests some basic functionality of the reporting sheet
    -> adding values with indicator report
    -> adding values with ristributino report
    -> converting to dataframe
    -> plotting
    """
    np.random.seed(23987)

    rs = ReportingSheet()

    # some artificial data
    u_rate = [3.3,3.2,3.1,3.1,3,3,3,3.1,3.1,3.2,3.2,3.3,3.3,3.3,3.4]
    gdp = [45723,46467,47787,44442,42107,41086,47959,46285,43858,46644,41531,41485,45427,41587,36323]

    rep_u = IndicatorReport("Time", "Unemployment",u_rate)
    rep_gdp= IndicatorReport("Time", "GDP",gdp)

    some_dist_x = np.random.rand(len(u_rate))
    some_dist_y = np.random.rand(len(u_rate))
    dist_xy = list(zip(some_dist_x,some_dist_y))
    rep_xy = DistributionReport("x","y", dist_xy)

    assert all([rep_u.data[i] == u_rate[i] for i in range(len(u_rate))])
    assert all([rep_gdp.data[i] == gdp[i] for i in range(len(gdp))])

    # first init method
    rs.add_report(rep_u)
    rs.add_report(rep_gdp)
    rs.add_report(rep_xy)

    rs.plot(show_plot=False)
    plt.close()

    # second init method
    rs2 = ReportingSheet(instances=[rep_u,rep_gdp,rep_xy])

    assert rs2.to_dataframe().loc[0,"GDP"] == gdp[0]
    assert rs2.to_dataframe().loc[0,"x"] == np.min(some_dist_x)

    # distribution reporting some_dist_x = np.random.rand(len(u_rate))
    some_dist_x = np.random.rand(500)
    some_dist_y = np.random.rand(500)
    dist_xy = list(zip(some_dist_x,some_dist_y))
    rep_xy = DistributionReport("x","y", dist_xy)
    rs3 = ReportingSheet(instances=[rep_xy])

    rs3.plot(show_plot=False)
    plt.close()


if __name__ == "__main__":
    test_reporting_sheet()
