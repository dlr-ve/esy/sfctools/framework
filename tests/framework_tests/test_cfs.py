from sfctools import Agent, CashFlowEntry

def test_cfs():
    """
    this is a test for the cash flow statement.
    It will write some elements and see if they can be read out correctly.
    """
    a = Agent()
    cfs = a.cash_flow_statement

    kind = CashFlowEntry.FINANCING
    tag = "My Subject"
    value = 10.0

    cfs.new_entry(kind,tag,value)
    cfs.new_entry(kind,tag,value-0.1)
    print(cfs.to_dataframe())

    value = cfs.get_entry(CashFlowEntry.FINANCING,"My Subject")
    assert value == 19.9

    value = cfs.get_entry(CashFlowEntry.FINANCING,"Some other")
    assert value == 0


    kind = CashFlowEntry.OPERATING
    tag = "Some other"
    cfs.new_entry(kind,tag,42.0)

    value = cfs.get_entry(CashFlowEntry.OPERATING,"Some other")
    assert value == 42.0



if __name__ == "__main__":
    test_cfs()
