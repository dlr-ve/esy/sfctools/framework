from sfctools import Agent, FlowMatrix, Accounts, BalanceEntry
import matplotlib as mpl
import matplotlib.pyplot as plt

import numpy as np

"""
This is a test for the transaction flow matrix. It generates some transactions
and checks if the matrix is consistently logged.
"""

# create some agent classes
class MyAgent(Agent):
 def __init__(self):
     super().__init__()
     with self.balance_sheet.modify:

         self.balance_sheet.change_item("Cash",BalanceEntry.ASSETS,+100.0,suppress_stock=True)
         self.balance_sheet.change_item("Equity",BalanceEntry.EQUITY,+100.0,suppress_stock=True)


class TypeA(MyAgent):
 def __init__(self):
     super().__init__()

class TypeB(MyAgent):
 def __init__(self):
     super().__init__()

class TypeC(MyAgent):
 def __init__(self):
     super().__init__()

class TypeD(MyAgent):
 def __init__(self):
     super().__init__()

class TypeE(MyAgent):
 def __init__(self):
     super().__init__()


def test_flowmatrix_creation():
    """
    test consistency
    """

    FlowMatrix().reset()
    
    my_a = TypeA() # create agents
    my_b = TypeB()
    my_c = TypeC()
    my_d = TypeD()
    my_e = TypeE()
    my_e2 = TypeE()

    def transfer(t,agent1,agent2,subject,quantity): # define a generic transaction
     with agent1.balance_sheet.modify:
         with agent2.balance_sheet.modify:

             agent1.balance_sheet.change_item("Cash",BalanceEntry.ASSETS,-quantity)
             agent1.balance_sheet.change_item("Equity",BalanceEntry.EQUITY,-quantity)

             agent2.balance_sheet.change_item("Cash",BalanceEntry.ASSETS,+quantity)
             agent2.balance_sheet.change_item("Equity",BalanceEntry.EQUITY,+quantity)

             FlowMatrix().log_flow(t, quantity, agent1, agent2 ,subject=subject)

    # do some transactions
    transfer((Accounts.CA, Accounts.CA),my_a,my_b,"my subject",quantity=42)
    transfer((Accounts.CA, Accounts.CA),my_a,my_c,"other subject",quantity=3.1415)
    transfer((Accounts.CA, Accounts.KA),my_c,my_d,"different subject",quantity=10.0)
    transfer((Accounts.KA, Accounts.KA),my_a,my_e,"no subject",quantity=3.1415)
    transfer((Accounts.KA, Accounts.KA),my_a,my_e2,"no subject",quantity=3.1415)

    print(FlowMatrix().to_dataframe())

    # check that total column sum equals total row sum
    my_matrix = FlowMatrix().to_dataframe(insert_nullsym=False)
    total_val1 = my_matrix.T["Total"].sum()
    total_val2 = my_matrix["Total"].sum()

    assert abs(total_val1 - total_val2) < 0.1, "%s != %s" % (total_val1,total_val2) # allow rounding errors

    val = my_matrix["TypeC"]["CA"].sum()
    val2 = my_matrix["TypeC"]["KA"].sum()
    print(val)
    print(val2)

    assert abs(val+13.716999999) < 1e-3
    assert abs(val2-13.717) < 1e-3



def disabled_test_flowmatrix_plot():
    # test plotting

    # output flow matrix as table and Sankey chart
    FlowMatrix().plot_colored(group=False,show_values=False,show_plot=False)
    FlowMatrix().plot_colored(group=True,cmap=mpl.cm.viridis,show_plot=False)

    my_colors = [mpl.cm.viridis(i) for i in np.linspace(0,1,6)]
    FlowMatrix().plot_sankey(colors=my_colors,show_values=False,show_plot=False)
    plt.close()



if __name__ == "__main__":
    test_flowmatrix()
    test_flowmatrix_plot()
