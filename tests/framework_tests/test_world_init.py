from sfctools import Agent,World

class AgentA(Agent):
    def __init__(self):
        super().__init__()

class AgentB(Agent):
    def __init__(self):
        super().__init__()

    def link(self):
        self.a_agents = World().get_agents_of_type("AgentA")
        print("test",self.a_agents)

def test_world():
    """
    tests the 'link' function of World
    """
    a = AgentA()
    b = AgentB()
    b2 = AgentB()

    World().link()

    assert len(World().get_agents_of_type("AgentA")) == 1
    assert len(World().get_agents_of_type("AgentB")) == 2

    print(b.a_agents)
    assert b.a_agents[0] == a


def test_world2():
    """
    tests singleton property of world
    """
    a = World()
    b = World()
    print(id(a), id(b))
    assert id(a) == id(b)


if __name__ == "__main__":
    test_world()
    test_world2()
