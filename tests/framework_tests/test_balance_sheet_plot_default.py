from sfctools import BalanceEntry,BalanceSheet
import matplotlib.pyplot as plt

def test_plot_custom():
    """
    tests custom colors for matplotlib barplot
    """

    bs = BalanceSheet(None)
    with bs.modify:
        bs.change_item("TestAsset", BalanceEntry.ASSETS, 30.0)
        bs.change_item("TestAsset2", BalanceEntry.ASSETS, 30.0)
        bs.change_item("TestAsset3", BalanceEntry.ASSETS, 40.0)
        bs.change_item("TestLiab", BalanceEntry.LIABILITIES, 70.0)
        bs.change_item("TestLiab2", BalanceEntry.LIABILITIES, 10.0)
        bs.change_item("TestEq", BalanceEntry.EQUITY, 1)
        bs.change_item("TestEq3", BalanceEntry.EQUITY, 1)
        bs.change_item("TestEq2", BalanceEntry.EQUITY, 18)


    colors_a = ["red","indianred"]
    colors_l = ["purple","lavender"]
    colors_e = ["black", "gray","lightgray"]

    plt.figure()
    bs.plot(cols_assets=colors_a, cols_liabs=colors_l,cols_eq=colors_e,ax=plt.gca())
    plt.close()

if __name__ == "__main__":
    test_plot_custom()
