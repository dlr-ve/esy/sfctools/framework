from sfctools import Clock, StockManager
import numpy as np


def test_stock():
    
    StockManager().register("capital","P_K",1.0) # register a new index 
    StockManager().register("apples","P_A",1.0) # register a second index 

    Clock().tick()
    
    for i in range(9): # run 10 iterations 
        
        #  price of 'K', 'P_K', is 10.0 plus a rising component
        StockManager().set_price("P_K",10.0 + 0.1*i) 
        Clock().tick()
    
    # access the stock manager and request the price history of 'P_K' 
    times, values = StockManager().request_price_history("P_K")
    
    assert list(times) == [0., 1., 2., 3., 4., 5., 6., 7., 8., 9.]
    assert list(values) == [ 1.] + [10+0.1*i for i in range(9)]
    
    
    times, values = StockManager().request_price_history("capital",human=True)
    
    assert list(times) == [0., 1., 2., 3., 4., 5., 6., 7., 8., 9.]
    assert list(values) == [ 1.] + [10+0.1*i for i in range(9)]


if __name__ == "__main__":
    test_stock()

