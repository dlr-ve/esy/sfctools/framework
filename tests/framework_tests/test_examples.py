

def test_examples():
    """
    tests if all examples run
    """

    from sfctools.examples import HelloWorldExample
    HelloWorldExample().run()

    from sfctools.examples import BalanceSheetExample
    BalanceSheetExample().run()

    from sfctools.examples import BankOrderBookExample
    BankOrderBookExample().run()

    from sfctools.examples import BasicExample
    BasicExample().run()

    from sfctools.examples import ClockExample
    ClockExample().run()

    from sfctools.examples import ABMExample
    ABMExample().run()

    from sfctools.examples import FlowMatrixExample
    FlowMatrixExample().run()

    from sfctools.examples import MarketExample
    MarketExample().run()

    from sfctools.examples import MarketExample2
    FlowMatrixExample().run()

    from sfctools.examples import MonteCarloExample
    MonteCarloExample().run()

    from sfctools.examples import SignalSlotExample
    SignalSlotExample().run()

    from sfctools.examples import InventoryExample
    InventoryExample().run()

    from sfctools.examples import ReadMeExample 
    ReadMeExample().run()
