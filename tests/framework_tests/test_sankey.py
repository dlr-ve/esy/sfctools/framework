
from sfctools.misc.mpl_plotting import plot_sankey
import matplotlib.pyplot as plt
import pandas as pd


def test_sankey():
    """
    """

    df1 = pd.DataFrame({
                       "from": ["A","A","B","A"],
                       "to": ["C","U","C","D"],
                       "value": [1.0,8.0,3.0,2.0],
                       "color_id":[0,0,0,1] })
    df2 = pd.DataFrame({
                        "from": ["C","C","C","D"],
                        "to": ["F","X","G","G"],
                        "value": [6.0,8.0,.8,5.5],
                        "color_id":[0,1,2,0] })
    df3 = pd.DataFrame({
                        "from": ["F","G","F","X","G","G"],
                        "to": ["H","I","J","H","H","J"],
                        "value": [.2,1.0,1.0,10.0,4.0,6.0],
                        "color_id":[0,2,3,4,2,0] })

    my_test_data = [df1, df2]

    fig = plot_sankey(my_test_data,show_values=True,show_plot=False)
    plt.close()


if __name__ == "__main__":
    test_sankey()
