from sfctools import Agent
from sfctools import BalanceEntry as BE

def test_changelog():
    """
    tests the changelog functionality of balance sheet
    """

    class Bank(Agent):
        """
        dummy bank
        """
        def __init__(self):
            super().__init__()


    class Depositor(Agent):
        """
        dummy depositor
        """
        def __init__(self):
            super().__init__()


    b = Bank()
    d = Depositor()


    b.balance_sheet.add_changelog(d,"Deposits",BE.LIABILITIES,10)
    b.balance_sheet.add_changelog(d,"Deposits",BE.LIABILITIES,-2)

    cl = b.balance_sheet.get_last_changelog("Deposits",BE.LIABILITIES,agent=d)

    assert cl == 8
