from sfctools.datastructs.worker_registry import WorkerRegistry
from sfctools import Agent 
import numpy as np 


def test_worker_registry():
    
    class Worker(Agent):
        def __init__(self):
            super().__init__()
            self.reservation_wage = np.random.rand()
            
    a = Agent()
    
    wreg = WorkerRegistry(owner=a,wage_attr="reservation_wage") # create a new worker registry 
    
    workers = [Worker() for i in range(10)] # create a bunch of workers 
    
    for worker in workers: # add workers to registry 
        wreg.add_worker(worker)
    
    costs = wreg.get_avg_costs() # get average reservation wage of all workers 
    wreg.fire_random(2) # fire two random workers 
    
    assert costs > 0
    assert len(wreg.agent_data) == 8
    
    w = wreg.pop()
    assert isinstance(w,Worker)
    

if __name__ == "__main__":
    test_worker_registry()