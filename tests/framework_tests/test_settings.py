import tempfile
from sfctools import Settings

def test_settings_read_write():
    # test the settings reading and writing mechanism

    teststr = """
metainfo:
    author: your name here
    date: 2022
    info: example settings

hyperparams:
    - name: example
      value: 42.0
      description: an example parameter

"""

    with open("tests/testsettings.yml","w") as tmp:
        tmp.write(teststr)

    Settings().read("tests/testsettings.yml")

    print(Settings().get_info())
    print(Settings())
    tmp.close()

    Settings().write("tests/testsettings-2.yml")



def test_settings_values():
    # test if the settings reading mechanism gets correct values
    sett = Settings().read("tests/testsettings.yml")
    assert sett["example"] == 42.0


if __name__ == "__main__":
    test_settings_read_write()
    test_settings_values()
