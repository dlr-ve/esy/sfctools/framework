from sfctools.core.singleton import Singleton


def test_singleton2():
    # tests singleton with arguments in constructor

    class ThisAgent(Singleton):
        def __init__(self, a=None, b=None):
            
            # super(ThisAgent, self.__class__).__init__(self)

            if self.do_init:
                self.a = a
                self.b = b

                self.set_initialized()

    class YourAgent(Singleton):

        def __init__(self):
            # super(YourAgent, self.__class__).__init__(self)
            pass 

    mine = ThisAgent(a=33, b=44) # .configure(3,4)
    mine.a = 4 
    mine.a = 4 

    yours = YourAgent()

    print(id(mine), id(yours))
    assert id(mine) != id(yours)

    other_mine = ThisAgent()
    other_mine.a = 3 
    other_mine.b = 3

    assert id(mine) == id(other_mine)

    assert mine.a == other_mine.a 
    assert mine.b == other_mine.b 


def test_singleton():
    # tests if singleton is actually singleton

    class MyAgent(Singleton):
        def __init__(self):
            # super().__init__()
            pass 

    s = Singleton()
    s2 = Singleton()

    assert id(s) == id(s2), "singletons do not have same id"

    s3 = MyAgent()
    s4 = MyAgent()

    assert id(s3) == id(s4), "singletons do not have same id"

    
if __name__ == "__main__":
    test_singleton()
    test_singleton2()

