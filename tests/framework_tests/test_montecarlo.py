
from sfctools.automation.runner import ModelRunner
import numpy as np
import pandas as pd


def test_montecarlo():
    """
    This tests the automation routines for monte-carlo batches
    """

    # run this in the upper directory via pytest tests/, otherwise this will fail
    settings_path =  "./tests/automation_test/settings.yml"
    results_path = "./tests/automation_test/results/"

    def builder():
        # do nothing here
        return None

    def iter(n): # one model iteration, repeated n times
        vals = []
        for i in range(n):
            vals.append(np.random.rand())

        return pd.DataFrame({"Value":vals}) # has to return a dataframe

    # create model runner
    mr = ModelRunner(settings_path,results_path,builder,iter)
    mr.run(10,20)

    with open(results_path+"output.txt","r") as file:
        text = file.read()
        print("TEXT", text)
        assert text.strip() == """author:your name here
date:2022
info:example settings
./tests/automation_test/results/output.0
./tests/automation_test/results/output.1
./tests/automation_test/results/output.2
./tests/automation_test/results/output.3
./tests/automation_test/results/output.4
./tests/automation_test/results/output.5
./tests/automation_test/results/output.6
./tests/automation_test/results/output.7
./tests/automation_test/results/output.8
./tests/automation_test/results/output.9""".strip()
