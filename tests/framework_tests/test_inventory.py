from sfctools.datastructs.inventory import Inventory
from sfctools import Agent 

def test_inventory():
    
    class InventoryAgent(Agent):
        def __init__(self):
            super().__init__()
            self.inventory = Inventory(self)
    
    
    my_agent = InventoryAgent()
    my_agent.inventory.add_item("Apple",1,3.0)
    my_agent.inventory.add_item("Apple",1,5.0)
    my_agent.inventory.remove_item("Apple", 1)
    w = my_agent.inventory.worth
    my_agent.inventory.add_item("Pear",2,2.0)
    my_agent.inventory.remove_item("Pear", 2)
    n_a = my_agent.inventory.get_inventory("Apple")
    n_p = my_agent.inventory.get_inventory("Pear")
    w2 = my_agent.inventory.worth

    assert w == 4.0
    assert n_a == 1.0
    assert n_p == 0.0
    assert w2 == 4.0


if __name__ == "__main__":
    test_inventory()