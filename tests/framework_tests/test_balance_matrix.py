
from sfctools import Agent, BalanceEntry, BalanceSheet
from sfctools import BalanceMatrix, World 
import numpy as np 

def test_balance_matrix():
    """
    does the balance matrix work properly
    """

    class OneAgent(Agent):

        def __init__(self):
            super().__init__()
        
        def modify(self,x):
            
            with self.balance_sheet.modify: 
                self.balance_sheet.change_item("MyAsset", BalanceEntry.ASSETS, x)
                self.balance_sheet.change_item("NetWorth", BalanceEntry.EQUITY, x)
    
    class OtherAgent(Agent):

        def __init__(self):
            super().__init__()
        
        def modify(self,x):
            
            with self.balance_sheet.modify: 
                self.balance_sheet.change_item("YourAsset", BalanceEntry.ASSETS, x)
                self.balance_sheet.change_item("NetWorth", BalanceEntry.EQUITY, x)
        

    # print(World().agent_registry)
    # World().reset()

    a = OneAgent() 
    b = OtherAgent()

    bm = BalanceMatrix() # initialize balance  matrix
    # bm.init_data()

    a.modify(10)

    BalanceMatrix().init_data()

    a.modify(3)
    b.modify(10)

    BalanceMatrix().fill_data() # fill balance matrix with data 

    df = BalanceMatrix().to_dataframe()
    print(BalanceMatrix().to_string(add_residual=False))
    
    assert abs(BalanceMatrix().to_dataframe().loc["NetWorth"]["Total"][0] - 13.0) < 1e-6
    assert abs(BalanceMatrix().to_dataframe().loc["YourAsset"][("OtherAgent", "A")] - 10.0) < 1e-6
    

if __name__ == "__main__":
    """
    """

    test_balance_matrix() 
    