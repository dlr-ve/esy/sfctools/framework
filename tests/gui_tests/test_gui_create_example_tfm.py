
from PyQt5 import QtCore
from PyQt5.QtCore import Qt

import pytest
# import os
# os.environ["QT_API"] = "pyqt5"
#from pytestqt.qt_compat import qt_api

from sfctools.gui.attune.src.qtattune import TransactionDesigner, MatrixViewer

import time


@pytest.fixture
def main_window_with_agents(qtbot):
    """
    tests the main window functionality
    this creates an instance of the main attune window and returns it
    """
    window = TransactionDesigner()

    # window.test_mode = False  # < disable matrix viewer popup

    def create_agent(name):
        qtbot.mouseClick(window.HelperNameEdit, Qt.LeftButton)
        for i in range(15):
            qtbot.keyPress(window.HelperNameEdit, Qt.Key_Backspace)

        qtbot.keyClicks(window.HelperNameEdit,name)
        qtbot.mouseClick(window.addButton2, Qt.LeftButton)

    # create three agents
    create_agent("A")
    create_agent("B")
    create_agent("C")


    def insert_text(widget,text):
        qtbot.mouseClick(widget, Qt.LeftButton)
        for i in range(25):
            qtbot.keyPress(widget, Qt.Key_Right)
        for i in range(25):
            qtbot.keyPress(widget, Qt.Key_Backspace)
            # time.sleep(.001)
        qtbot.keyClicks(widget,text)

    # fill the parameters
    insert_text( window.agent1Edit, "A")
    insert_text( window.agent2Edit, "B")
    insert_text( window.AssetListLeft, "+ Deposits")
    insert_text( window.LiabilityListLeft, "+ Loans")
    insert_text( window.EquityListLeft, "")
    insert_text( window.AssetListRight, "+ Loans")
    insert_text( window.LiabilityListRight, "+ Deposits")
    insert_text( window.EquityListRight, "")
    insert_text( window.editShortname, "transaction1")
    insert_text( window.editSubject, "loans")
    insert_text( window.editQuantity, "d")
    insert_text( window.editDescription, "my description")
    qtbot.keyClicks(window.registerFlowBox, "False")
    qtbot.keyClicks(window.registerFlowBox, "False")

    time.sleep(2.15)

    # assert window.registerFlowBox.currentText() == "False" # True = index 0 here

    # first transaction
    qtbot.mouseClick(window.addButton, Qt.LeftButton)

    # second transaction
    insert_text( window.agent1Edit, "A")
    insert_text( window.agent2Edit, "C")
    insert_text( window.AssetListLeft, "- Deposits")
    insert_text( window.LiabilityListLeft, "")
    insert_text( window.EquityListLeft, "- Equity")
    insert_text( window.AssetListRight, "+ Deposits")
    insert_text( window.LiabilityListRight, "")
    insert_text( window.EquityListRight, "+ Equity")
    insert_text( window.editShortname, "transaction2")
    insert_text( window.editSubject, "consumption")
    insert_text( window.editQuantity, "x")
    insert_text( window.editDescription, "my other description")
    qtbot.keyClicks(window.registerFlowBox, "True")
    qtbot.keyClicks(window.registerFlowBox, "True")

    time.sleep(2.15)

    # assert window.registerFlowBox.currentText() == "True" # True = index 0 here
    qtbot.mouseClick(window.addButton, Qt.LeftButton)

    time.sleep(.15)


    return window



def test_tfm(main_window_with_agents,qtbot):
    """
    test if the flow matrix is compiled correctly
    """

    # click the generate transaction flow matrix button
    main_window_with_agents.genMatButton.trigger()
    mw = main_window_with_agents


    time.sleep(0.05)

    mv = MatrixViewer.instance
    time.sleep(.1)

    assert mv is not None

    data = mv.get_data()
    time.sleep(2)

    # cross-check the resulting matrix

    assert data.loc["Total"]["A (KA)"] == "", data.loc["Total"]["A (KA)"]
    assert data.loc["Total"]["B (KA)"] == "", data.loc["Total"]["B (KA)"]
    assert data.loc["Total"]["C (KA)"] == "", data.loc["Total"]["C (KA)"]

    time.sleep(.1)
