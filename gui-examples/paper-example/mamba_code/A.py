$[AGENT] A

+[INIT]
+[ACCOUNTING] # assign some initial wealth
<.> "Deposits",ASSETS,1000.0
<.> "Equity",EQUITY,1000.0
+[ENDACCOUNTING]
+[ENDINIT]

+[KNOWS] C
+[PARAM] x

+[FUN] consume
<~~> transaction1(self,@C[0],$.x)
+[ENDFUN]

$[END]
