
from sfctools import World, Agent, Clock, Settings, BalanceEntry, ICSEntry
import numpy as np

from .transactions import *


class B(Agent):

		def __init__(self,*args):
			super().__init__()
		
			self.A = None


		def link(self):

			self.A = World().get_agents_of_type('A')

			self.d = Settings().get_hyperparameter('d')

		
		def grant_loan(self):
			transaction2(self,self.A[0],self.d)
		

