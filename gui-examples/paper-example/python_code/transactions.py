from sfctools import FlowMatrix 
import numpy as np
from sfctools import BalanceEntry,Accounts
from sfctools import CashFlowEntry
from sfctools import ICSEntry
CA = Accounts.CA
KA = Accounts.KA

def transaction1(A, C, q):
    #
    cond = (isinstance(q,float) or isinstance(q,int))
    if cond: assert not np.isnan(q)
    if cond: assert q < +np.inf
    if cond: assert q > -np.inf
    if cond: assert q >= 0, 'have to pass positive quantity: unidirectional transaction'
    A.balance_sheet.disengage()
    C.balance_sheet.disengage()
    A.balance_sheet.change_item('Equity', BalanceEntry.EQUITY, -q, suppress_stock=False)
    A.balance_sheet.change_item('Deposits', BalanceEntry.ASSETS, -q, suppress_stock=False)
    C.balance_sheet.change_item('Equity', BalanceEntry.EQUITY, +q, suppress_stock=False)
    C.balance_sheet.change_item('Deposits', BalanceEntry.ASSETS, +q, suppress_stock=False)
    A.balance_sheet.engage()
    C.balance_sheet.engage()
    FlowMatrix().log_flow((KA,KA), q, A, C, subject='Consumption')


def transaction2(B, A, q):
    #
    cond = (isinstance(q,float) or isinstance(q,int))
    if cond: assert not np.isnan(q)
    if cond: assert q < +np.inf
    if cond: assert q > -np.inf
    if cond: assert q >= 0, 'have to pass positive quantity: unidirectional transaction'
    B.balance_sheet.disengage()
    A.balance_sheet.disengage()
    B.balance_sheet.change_item('Loans', BalanceEntry.ASSETS, +q, suppress_stock=False)
    B.balance_sheet.change_item('Deposits', BalanceEntry.LIABILITIES, +q, suppress_stock=False)
    A.balance_sheet.change_item('Deposits', BalanceEntry.ASSETS, +q, suppress_stock=False)
    A.balance_sheet.change_item('Loans', BalanceEntry.LIABILITIES, +q, suppress_stock=False)
    B.balance_sheet.engage()
    A.balance_sheet.engage()


