
"""
This the main ABM Simulation file
Cretaed with MAMBA GUI

@author: <Your name>
@date: <Your date>
"""

from sfctools import Agent, World, Settings, Clock, FlowMatrix
from python_code.a import A


def iter():
    """
    this is one iteration
    """

    # TODO modify iteration here
    for a in World().get_agents_of_type('A'):
        print('Do something with' + str(a))



def run():
    """
    this is the main simulation loop
    """



    Settings().read("python_code/settings.yml") # read settings file

    """
    Simulation parameters
    """

    # number of agents to be created
    N_A = 1

    # TODO^ set the correct value

    # number of simulation steps
    T = 100

    # TODO^ set the correct values

    # create Agents:
    [A() for i in range(N_A)]

    # inter-link agents 
    World().link()


    for i in range(T):
        iter()

        # TODO write outputs here ...

        Clock().tick()

    print(FlowMatrix().to_string())
