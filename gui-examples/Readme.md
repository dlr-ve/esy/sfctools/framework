# Welcome to the GUI Example Page


The basic working principle for GUI projects is the following

 - Get the project files from the gitlab repository containing the example folders

 - Open sfctools-attune from a Python command prompt: 'python -m sfctools attune'

 - Navigate to the project file (ending with .sfctl) by clicking File > Browse...

 - The model graph should become visible in the central graph widget

 - Click Scripts>Edit Main loop to view the source code, and view other model features interactively

 - Click Scripts>Run Main loop in order to run the project in a new console window.
