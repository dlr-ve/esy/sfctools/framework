Framework Structure 
======================

Sfctools is designed in a relatively flat architecture. It combines different sub-modules.
The features can be categorized into data structures for agents, bottom-up modeling tools and managerial (parameters and data-related) features.

.. list-table:: overview - sfctools
   :widths: 25 25 50
   :header-rows: 1

   * - Automation and Parameters
     - Bottom-Up Tools
     - Data Structures 

   * - Yaml hyperparameters
     - Flow matrix     
     - Income statement

   * - Clock 
     - World (registry)         
     - Cashflow statement 

   * - Stock manager    
     - Balance sheet  
     - Inventory  

   * - Model runner    
     - Market registry
     - Worker registry          

   * - 
     - Decentralized matching    
     - Bank order book   

   * - 
     - Production tree
     - Signals and slots
    
More information can be found in the API handbook section.