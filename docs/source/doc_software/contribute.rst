Ongoing Development
========================

.. note:: 
  This is an open-source package. Feel free to contribute at any time :)


How to Contribute: General Information 
---------------------------------------
Sfctools is open for your contribution. If you are interested in contributing to the code or the documentation, you are greatfully invited to do so! 
If you want to contribute to this repository, please discuss the changes you wish to make, or new features you want to introduce, with the core development team.
Please also try to adapt your code style in accordance with the coding conventions and quality values of this repository.
Read the Contributors License Agreement (CLA) and send a signed copy to `Thomas Baldauf <mailto:thomas.baldauf@dlr.de>`_.

The current development status is available on our Gitlab repository: https://gitlab.com/dlr-ve/esy/sfctools

If you wish to make a small bug report or want to report an issue, open up a new issue in Gitlab (preferred) or contact the main developer (`Thomas Baldauf <mailto:thomas.baldauf@dlr.de>`_)


Release Timeline
----------------

Dated 10/2022.

.. list-table:: Release timeline for past and planned future releases
   :widths: 5,5,5,5
   :header-rows: 1

   * - Planned Quarter
     - Version Number
     - Short Name 
     - (Planned) Features

   * - Q3-2021
     - 0.3 - Alpha release
     - Michelada
     - Core functionalities

   * - Q4-2021
     - 0.4 - Beta release
     - Chicha 
     - | batch functionality,
       | first example projects 

   * - Q1-2022
     - 0.5 - Beta release
     - Xocolatl
     - | improved testing ,
       | additional example projects

   * - Q1-2022 to Q3-2022 
     - 0.9.9 - Beta release (internal)
     - | Kwas
     - | release-ready (before publication),
       | first viable product, including the graphical user interface (before review)

   * - Q4-2022
     - 1.0.x - Stable beta release
     - | Kwas
     - | release-ready (befure publication),
       | first viable product, including the graphical user interface (before review)
       | improvements after first feedback

   * - Q2-2023
     - 1.1.x - Paper release
     - | Spotykach
     - | peer-reviewed version, JOSS publication

   * - Q4-2023 or later\*
     - 1.2.x - (Community release)
     - | (Lassi)
     - | first patches and feature requests from users,
       | additional details


\*) ongoing or future release


Maintenance Plan 
-----------------
The software will be updated in form of bug-fixes and feature extensions. However, the time schedule of future updates is not clearly determined yet. Please be aware that we do not give any warranty about the code provided in the alpha and beta release phases.

Quality Values
-----------------

Dated 11/2021.

.. list-table:: Quality values
   :widths: 35, 35
   :header-rows: 1

   * - Value 
     - Description
   * - Learnability
     - | The highest priority of sfctools is to provide an
       | easy-to-apply tool for scientists (not primarily developers).
       | Hence, easy to understand approaches, code structures 
       | and explicit formulations are favored.
   * - Readability
     - | The readability of the code is a key aspect.
       | Splitting of statements is favored over convoluted one-liners.
   * - Reliability
     - | The provided tools should be reliable 
       | and stable (if not, a beta release warning should be added).
   * - Performance
     - | Agent-based simulation tools are anyways slower than other modeling approaches. 
       | Therefore, performance should only be optimized where necessary.

Coding Conventions
----------------------------

Dated 11/2021.

- mainly PEP-8 coding style https://www.python.org/dev/peps/pep-0008/
- exception are economic and mathematical variables that have capital letters for convenience 


Why Python?
-------------

Python is the ’programming language of engineers’ and follows the philosophy of open source. It opens the
world of programming to engineers and non-programming specialists via an easy installation and import of
modules, shipping powerful toolboxes in an easy-to-read syntax. The aim of providing this framework in Python
is to lower the entry barrier for economists and engineers to macroeconomic computer modeling, and, at the
same time, to allow for the design of highly flexible, custom-built simulations.

The simplified language for sfctools-attune is a description language we have designed to facilitate agent-based computational economics to a broad community.
For the sake of faster prototyping, as well as easier handling of the agent-based modeling aspects, we have introduced this language as part of the graphical user interface.
Although the full capabilities of sfctools can also be reached using full Python code, in our view, the attune language offers a fast(er) and (more) efficient approach to SFC-ABM modeling.


Organizational Information 
--------------------------

Core Development Team 
^^^^^^^^^^^^^^^^^^^^^^

The core development team of sfctools is listed in the table below (dated 10/2022).

.. list-table:: 
   :widths: 35, 35
   :header-rows: 1

   * - Role
     - Responsible
   * - Scrum Master
     - Benjamin Fuchs [#f1]_ 
   * - Developer 
     - Thomas Baldauf [#f2]_
   * - Product Owner 
     - Marlene O`Sullivan (2020-2022) [#f2]_ Patrick Jochem (2022-now) [#f1]_

.. [#f1]  Department of Energy System Analysis at Institute of Networked Energy Systems, DLR 
.. [#f2]  Energy Economics Group, Department of Energy System Analysis at Institute of Networked Energy Systems, DLR 


Open-Source Readiness 
^^^^^^^^^^^^^^^^^^^^^
| Features, dependencies and components which are contraindicative or at odds
| with an open source publication should not be part of this package.

