Context and Scope
============================

Agent-Based Computational Economics
------------------------------------

One of the most challenging tasks in macroeconomic models is to describe the macro-level effects from the collective behavior of meso- or micro-level actors. Wheras in 1759, Adam Smith was still making use of the concept of an 'invisible hand' ensuring market stability and economic welfare, a more and more popular approach is to make the 'invisible' visible and to accurately model each actor individually by defining its behavioral rules and myopic knowledge domain. In agent-based computational economics (ACE), economic actors correspond to dynamically interacting entities (also called agents) who live inside a computer program. For many research topics, it is useful to combine ABM with the stock-flow consistent (SFC) paradigm. SFC-ABM models, however, are often intransparent and rely on very peculiar, custom-built data structures. A tedious task is to generate, maintain and distribute code for agent-based models  (ABMs), as well as to check for the inner consistency and logic of such models.

**Agent-based computational economics** is a modeling approach where independent myopic units, called agents, interact. The outcome of this interaction (called emergence) can be a self-organized pattern much more complicated than the individual agents’ behavioral rules :cite:p:`gatti2008emergent,gaffeo2008adaptive`. In Agent-Based Computational Economics, the agents are the types economic actors, such as individual firms or plant operators, performing certain operations such as
investment or bidding at markets. There is, however, a large plurality of different agent-based economic models.
Agent-based *macroeconomic* models are typically constructed of households, firms, banks, the government
and a central bank, who are either aggregate entities or interact in a bottom-up fashion :cite:p:`turrell2016agent`. A more detailed elaboration on agent-based comutational eonomics can be found, for example, in the two books: :cite:p:`tesfatsion2006handbook,gallegati2017introduction`.


Stock-Flow Consistency
----------------------

The appealing framework of sfctools builds around the Stock-Flow-Consistency (SFC) principle. The latter is a main
feature of state-of-the-art macroeconomic (agent-based) models :cite:p:`zezza2019design,caiani2016agent, reissl2021heterogeneous`. It originates
from the pioneering work of Copeland :cite:p:`copeland1949social` and later Dos Santos :cite:p:`dos2008simplified` and has since then found application in
many modeling exercises. The principle of stock-flow consistency applies to a wide spectrum of economic
streams, ranging from Hydraulic Keynesianism :cite:p:`andresen1998macroeconomy` to Post-Keynesianism :cite:p:`caverzasi2015post` and other schools of thinking. [#f1]_
It is therefore a versatile and widely applicable approach. The main idea is that the stocks (changes within an agent) and the flows (between agents) have to be consistent, just as stated in the Laws of Thermodynamics.
The concept is therefore especially suitable for ecological macroeconomic models :cite:p:`dafermos2014ecological`.

.. [#f1] For an essay on economic modeing streams, have a look at :cite:p:`dosi2019more`.

What can I do wih sfctools?
---------------------------

Agent-based modeling is a powerful approach utilized in economic simulations to generate complex dynamics, endogenous business cycles and market disequilibria. **sfctools** is an ABM-SFC modeling suite, which i) relies on transparent and robust data structures for economic agents, ii) comes along with a simple descriptive modeling language, iii) provides an easy project builder for Python, making the software runnable and accessible on a large number of platforms, and iv) is manageable from a graphical user interface for ABM-SFC modeling, shipped as part of the suite, assuring analytical SFC-check and double accounting consistency. The package is shipped in the form of an open-source project. Unlike more generic frameworks like *mesa* or *AgentPy*, it concentrates on agents in economics. 
*sfctools* was designed to be used by both engineering-oriented and economics-oriented scholars who have basic education in both fields. It can be used by a single developer or by a small development team, splitting the work of model creation in terms of consistency and economic logic from the actual programming and technical implementation. This allows software solutions from rapid prototyping up to sophisticated, small-to-medium-sized ABMs. It is therefore a versatile virtual laboratory for agent-based economics.

The **framework sfctools** will accompany your modeling work along the whole model design process. Typically modelers will start with constructing their agents, i.e. the transactions between agents and their behavioral parameters. 
Sfctools supports modelers with a **basic Agent class**. All agents which inherit from this class will automatically be equipped with datastructures, the most important being the balance sheet. A **flow matrix** sheet collects all cash flows between agents, as well as changes in stocks. 
Structural **model parameters** can be read from a **simple yaml file** to avoid hard-coding. Finally, sfctools will take care about timing your simulation periods and executing batch simulation runs.

.. image:: blocks.png
   :width: 800px
   :alt: alternate text
   :align: left

Literature References
---------------------- 

.. bibliography:: ../literature_architecture.bib
   :cited:

