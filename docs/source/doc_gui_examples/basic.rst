Paper Example for GUI
=====================

Let us consider a simple model example. namely a three-agent model consisting of a consumer agent A, a bank (B) and a consumption good producer (C). We employ two transactions (Figure ): in the first transaction, B grants a loan to A. Subsequently, A uses its bank deposits to obtain some goods at C. In this simple model, the first transaction only affects the stocks, whereas the second transaction (consumption) is an actual flow. For this, we use the graphical user interface *sfctools-attune*.


Setting up the model
--------------------

The model creation workflow is as follows

1. Set up the agents: we add three boxes for agents A,B and C to the model graph (*new box ---> Add*). Each box will contain a construction plan for an agent (a class in programming jargon)
2. Set up the transactions. This is done by entering the balance sheet modifications and adding them to the transcation list (*---> Add new*).

The first transaction alters the deposits as well as the loans:

Agent A is a consumer and is affected both by transaction 1 and by transaciton 2:

+---------------+------------+-------------+
|               | Assets     | Liabilities |
+---------------+------------+-------------+
| transaction 1 | +Deposits  | +Loans      |
+---------------+------------+-------------+
| transaction 2 | -Deposits  | -Equity     |
+---------------+------------+-------------+

Agent B is a bank and is affected only by transaciton 1:

+---------------+------------+-------------+
|               | Assets     | Liabilities |
+---------------+------------+-------------+
| transaction 1 | +Loans     | +Deposits   |
+---------------+------------+-------------+

Agent C is a consumption good producer and is affected only by transaction 2:

+---------------+------------+-------------+
|               | Assets     | Liabilities |
+---------------+------------+-------------+
| transaciton 2 | -Deposits  | -Equity     |
+---------------+------------+-------------+

Once the transactions are registered in the project, they can be deliberately used during the simulation by importing them from the *transactions.py* file

3. Generate transaction flow matrix: to ensure our model is fully stock-flow consistent, we check if all rows and columns of the transaction flow matrix sum up to zero: We obtain the matrix from (*Analysis Tools ---> Generate FlowMatrix*)

+---------------------+---------+----+----+-------+
|                     | A       | B  | C  | Total |
+---------------------+---------+----+----+-------+
| Consumption         | -x      | 0  | +x | 0     |
+---------------------+---------+----+----+-------+
| \Delta Deposits     | -d+x    | +d | -x | 0     |
+---------------------+---------+----+----+-------+
|   \Delta  Loans     | +d      | -d | 0  | 0     |
+---------------------+---------+----+----+-------+
| Total               | 0       | 0  | 0  | 0     |
+---------------------+---------+----+----+-------+


3. Insert behavioral rules for the agents:

+---------+----------------------------------------------------+
| Agent A | consume: get consumption goods from C              |
+---------+----------------------------------------------------+
| Agent B | grant_loan: bank grants loan to an agent of type A |
+---------+----------------------------------------------------+
| Agent C | (no internal functionality)                        |
+---------+----------------------------------------------------+

5. By exporting our model to python code via saving the project from the GUI (*File ---> Build and Save*), we automatically generate a fully consistent model, usable in any python script.

Writing the code
----------------

Thanks to the user friendliness of sfctools, there is little work to be done in terms of coding. In the gui, we code the three agents in the sfctools-attune language, a custom-designed agent description language for *sfctools-attune*.

Agent A:

.. code-block::

    $[AGENT] A

    +[INIT]
    +[ACCOUNTING] # assign some initial wealth
    <.> "Deposits", ASSETS, 100.0
    <.> "Equity", EQUITY, 100.0
    +[ENDACCOUNTING]
    +[ENDINIT]


    +[KNOWS] C
    +[FUN] consume
    x = BALANCE?("Deposits",ASSETS)-500.0
    <~~> transaction1(self,@C[0],x)
    +[ENDFUN]
    $[END]

Agent B:

.. code-block::

    $[AGENT] B
    +[KNOWS] A
    +[FUN] grant_loan
    d = 10
    <~~> transaction2(self,@A[0],d)
    +[ENDFUN]
    $[END]

Agent C:

.. code-block::

    $[AGENT] C
    +[INIT]
    # do nothing
    +[ENDINIT]
    $[END]

The latter code yields a full model description and encapsulates the inner functionality of our system. The main simulation loop is formulated in pure Python

.. code-block::

    from transactions import *  # import the pre-templated transactions
    from python_code.a import A # import the necessary agents
    from python_code.b import B
    from python_code.c import C
    from sfctools import World

    # create some instances of agents
    my_a_agent = A()
    my_b_agent = B()
    my_c_agent = C()

    World().link()

    # define one model iteration
    def iter():
        my_b_agent.grant_loan()
        my_a_agent.consume()

    # define model loop wrapper
    def run():
        for i in range(10): # run 10 iterations
            iter()

    # start model
    run()

    # import the FlowMatrix from sfctools
    from sfctools import FlowMatrix

    # print the flow matrix
    print(FlowMatrix().to_string())

The printed output should be the same as on the `paper example page <https://sfctools-framework.readthedocs.io/en/latest/doc_gui_examples/example_paper.html>`_.
