GUI Examples
==============

This the example page for the graphical user interface. The example folder can also be found on gitlab: https://gitlab.com/dlr-ve/esy/sfctools/framework/-/tree/main/gui-examples


.. toctree::

   basic.rst
