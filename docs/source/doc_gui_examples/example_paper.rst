Example from Paper
==================

This is a simple demonstration example involving three agents. To run the paper example, follow these steps:

 - Read more about the paper example in the paper (JOSS doi xxx-xxx-xxx)

 - Install sfctools (see `instruction page <https://sfctools-framework.readthedocs.io/en/latest/doc_intro/installation.html>`_)

 - Download the example from `gitlab <https://gitlab.com/dlr-ve/esy/sfctools/framework/-/tree/main/paper-example>`_ into your file system.

  Your folder structure should look like this

   .. code-block::

      .example_folder
      |____ . python_code
      |    |
      |    |__ a.py
      |    |__ b.py
      |    |__ c.py
      |    |__ mainloop.py
      |    |__ transactions.py
      |
      |____.
      |    | README.md
      |    | run_example.py
           | settings.yml


 - To start the example, switch to the example folder and run

    .. code-block::

       python run_example.py

 - Your output should be a print of the FlowMatrix from the paper example


    .. code-block::

          [sfctools]  UserWarning: Changed settings parameter d [settings.py LINE 285]
          [sfctools]  UserWarning: Changed settings parameter x [settings.py LINE 285]
                              A                   B                   C               Total
                             CA        KA        CA        KA        CA        KA
          Consumption     .-           -x     .-        .-        .-            x     .-
          Δ Deposits      .-       -d + x     .-            d     .-           -x     .-
          Δ Loans         .-            d     .-           -d     .-        .-        .-
          Total           .-        .-        .-        .-        .-        .-        .-


Comments: The first two warnings are caused by overwriting the default values of the settings.yml file with symbolic expressions, and can be ignored.
The columns KA and CA are default and stand for capital account and current account. They can be neglected in this example.
