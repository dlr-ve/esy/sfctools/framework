Installation
============================

Installation with pip
---------------------

Sfctools runs best on *Python 3.8*. The installation is very simple. Open up a command line window (ideally Anaconda prompt or mambaforge prompt) and type

.. code-block:: python

    python -m pip install sfctools

Check your working installation with

.. code-block:: python

    python -m pip show sfctools

For advanced users, it is recommended to create a new python environment for sfctools.

The GUI is part of the python package sfctools and can be simply called via

.. code-block:: python

    python -m sfctools attune

the GUI requires the graphics toolkit PyQt5, which is automatically installed with sfctools.

.. warning::

  This is a peer-reviewed piece of software, but can still be considered an early-stage release. Bug reports and feature requests are welcome!


Sfctools should run in any python >3.6 environment. 3.8 is the recommended version.

Troubleshooting
-----------------

Before the installation, please upgrade your current version of pip (22 or higher).

You have to use a Python version which is compatible. We recommend to create an environment with Python 3.8. For example, in anaconda:

.. code-block:: python

    conda create -n sfcenv python=3.8
    conda activate sfcenv
    (optional in pre-existing environmnent)  pip cache purge
    (optional in fresh environment) conda install pip
    pip install sfctools

You might have to install the latest version of pyyaml (we require 5.0 or higher) as well as of PyQt5.
The following commands could be helpful for troubleshooting

.. code-block:: python

    pip install --upgrade pip --user
    pip install --no-cache-dir --ignore-installed PyYAML
    pip install --no-cache-dir --ignore-installed PyQt5

If you are still having trouble of any kind during installation, please do not hesitate to contact the development team or open up an issue on gitlab.


Installation via the GitLab repository
---------------------------------------

This method is only recommended for advanced users with experience in software development.
Find the repository on the following page https://gitlab.com/dlr-ve/esy/sfctools

The corresponding files can be copied using 'git clone', see https://docs.gitlab.com/ee/user/ for a beginner guide.
The files can be locally packaged using poetry, see https://python-poetry.org.
