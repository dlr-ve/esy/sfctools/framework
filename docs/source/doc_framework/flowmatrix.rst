Flow Matrix
============

The `Flow Matrix <http://dx.doi.org/10.13140/RG.2.1.2924.9524>`_ is the most central component of stock-flow consistent economcis. 

.. automodule:: sfctools.core.flow_matrix
   :members:
   :undoc-members:
   :show-inheritance:
