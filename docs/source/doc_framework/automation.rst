Automation
============

This is a set of automation features for Monte-Carlo and batch runs.

.. note::
   The automation part is still rudimentary in the current release.

Model Runner
--------------

.. automodule:: sfctools.automation.runner
   :members:
   :undoc-members:
   :show-inheritance:
