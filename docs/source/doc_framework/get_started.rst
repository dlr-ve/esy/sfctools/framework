Getting Started 
============================

The framework can be used directly in Python by importing from sfctools, for example 

.. code-block:: 

    from sfctools import Agent

    from sfctools import Settings, FlowMatrix

    from sfctools import BalanceSheet 
    from sfctools import BalanceEntry as BE 

    from sfctools import IncomeStatement 
    from sfctools import ICSEntry as IE 


further sub-modules are explained in the API Handbook or in the individual examples. 