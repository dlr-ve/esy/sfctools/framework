Markets
================

The data structures provided here are a basis for agent-based market simulations. For example, they could be used in 
`financial market models <https://www.sciencedirect.com/science/article/pii/S0378437101003120>`_ or `labor market models <https://doi.org/10.1142/S0219525904000147>`_. 

Market Registry
-----------------

.. automodule:: sfctools.datastructs.market_registry
   :members:
   :undoc-members:
   :show-inheritance:   


Market Matching
-----------------

.. automodule:: sfctools.bottomup.matching 
   :members: 
   :undoc-members:
   :show-inheritance:
