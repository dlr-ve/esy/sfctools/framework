World and Settings
===================

World
-----------

.. automodule:: sfctools.core.world
   :members:
   :undoc-members:
   :show-inheritance:

Settings (yaml)
----------------

.. automodule:: sfctools.core.settings
   :members:
   :undoc-members:
   :show-inheritance:

Clock
-----------

.. automodule:: sfctools.core.clock
   :members:
   :undoc-members:
   :show-inheritance:

Stock Manager
----------------

.. automodule:: sfctools.bottomup.stock_manager
   :members:
   :undoc-members:
   :show-inheritance: