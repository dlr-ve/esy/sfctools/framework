Miscellaneous
===================

This is a collection of various additional features. Among others, time series manipulation and plotting.


Time series
-----------

.. automodule:: sfctools.misc.timeseries
   :members:
   :undoc-members:
   :show-inheritance:



Plotting
-----------

 .. autofunction:: sfctools.misc.mpl_plotting.matplotlib_barplot

 .. autofunction:: sfctools.misc.mpl_plotting.matplotlib_lineplot

 .. autofunction:: sfctools.misc.mpl_plotting.plot_sankey



Reporting
-----------

.. automodule:: sfctools.misc.reporting_sheet
   :members:
   :undoc-members:
   :show-inheritance:
