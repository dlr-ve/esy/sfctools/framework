Economic Agents
===============

An `Agent <https://en.wikipedia.org/wiki/Software_agent>`_ is an autonomous economic entity. Each agent is equipped with a balance sheet and an income statement.


Agents
-----------

.. automodule:: sfctools.core.agent
   :members:
   :undoc-members:
   :show-inheritance:

Balance Sheet
---------------

.. automodule:: sfctools.datastructs.balance
   :members:
   :undoc-members:
   :show-inheritance:

Income Statement
----------------

.. automodule:: sfctools.datastructs.income_statement
   :members:
   :undoc-members:
   :show-inheritance:

Cashflow Statement
-------------------

.. automodule:: sfctools.datastructs.cash_flow_statement
   :members:
   :undoc-members:
   :show-inheritance:
