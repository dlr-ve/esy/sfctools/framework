Additional Data Structures
==========================

Additional data structures are special data structures such as an inventory, a registry for workers or registry for debtors.
They are not included in the 'standard' instances, and thus not inherited from Agent.

Inventory
----------

.. automodule:: sfctools.datastructs.inventory
   :members:
   :undoc-members:
   :show-inheritance:

Worker Registry
-----------------

.. automodule:: sfctools.datastructs.worker_registry
   :members:
   :undoc-members:
   :show-inheritance:

Bank Order Book
-----------------

.. automodule:: sfctools.datastructs.bank_order_book
   :members:
   :undoc-members:
   :show-inheritance:

Production Tree
----------------

.. automodule:: sfctools.bottomup.productiontree
   :members:
   :undoc-members:
    :show-inheritance:


Signals and Slots
------------------

.. automodule:: sfctools.datastructs.signalslot
   :members:
   :undoc-members:
   :show-inheritance:
