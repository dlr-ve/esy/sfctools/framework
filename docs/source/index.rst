Welcome to the sfctools documentation page!
===========================================

.. image:: https://readthedocs.org/projects/sfctools-framework/badge/?version=latest
   :target: https://sfctools-framework.readthedocs.io/en/latest/?badge=latest
   :alt: Documentation Status

.. image:: https://img.shields.io/pypi/v/sfctools
   :alt: PyPI

.. image:: https://img.shields.io/pypi/pyversions/sfctools
   :alt: PyPI - Python Version

.. image:: https://img.shields.io/pypi/l/sfctools
   :alt: PyPI - License

.. image:: https://joss.theoj.org/papers/10.21105/joss.04980/status.svg
   :target: https://doi.org/10.21105/joss.04980


**sfctools** is an ABM-SFC modeling suite for agent-based macroeconomic, stock-flow consistent (ABM-SFC) modeling.
Unlike more generic frameworks like `mesa <https://mesa.readthedocs.io/>`_ or `AgentPy <https://agentpy.readthedocs.io/>`_, it concentrates on agents in economics. It further is a toolbox which helps you to construct agents,
helps you to ensure stock-flow consistency and facilitates basic economic data structures (such as the balance sheet).

   .. image:: title.png
      :width: 600px
      :alt: alternate text
      :align: left

.. toctree::
   :maxdepth: 1
   :caption: Getting Started

   doc_intro/installation

Quick start:

   pip install sfctools

.. toctree::
   :maxdepth: 1
   :caption: About This Software

   doc_software/software
   doc_software/structure
   doc_software/contribute


You can cite the software as follows: 

Baldauf, T., (2023). sfctools - A toolbox for stock-flow consistent, agent-based models. Journal of Open Source Software, 8(87), 4980, `https://doi.org/10.21105/joss.04980 <https://doi.org/10.21105/joss.04980>`_

You can cite the software repository as follows:

Thomas Baldauf. (2023). sfctools - A toolbox for stock-flow consistent, agent-based models (1.1.0.2b). Zenodo. `https://doi.org/10.5281/zenodo.8118870 <https://doi.org/10.5281/zenodo.8118870>`_

GitLab Software repository: click  `https://gitlab.com/dlr-ve/esy/sfctools/framework <https://gitlab.com/dlr-ve/esy/sfctools/framework>`_


.. toctree::
   :maxdepth: 1
   :caption: API Handbook

   doc_framework/get_started
   doc_framework/agents
   doc_framework/datastructs
   doc_framework/flowmatrix
   doc_framework/world_settings
   doc_framework/markets
   doc_framework/automation
   doc_framework/miscellaneous

.. toctree::
   :maxdepth: 1
   :caption: Examples

   doc_gui_examples/example_paper.rst

   doc_api_examples/examples_framework.rst

   doc_gui_examples/examples_gui.rst


.. toctree::
  :caption: sfctools-attune GUI

  doc_attune/workflow
  doc_attune/get_started

.. toctree::
  :caption: sfctools-attune Language

  doc_attune_language/attune_language_main


Contact
=============
- Author: Thomas Baldauf
- Contact: `thomas.baldauf@dlr.de <mailto:thomas.baldauf@dlr.de>`_
