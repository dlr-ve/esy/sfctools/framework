sfctools-attune Language Basics
================================
This page explains the descriptive language for agents in *sfctools-attune*. It is by no means another programming language. In principle, it works like Python (and is also converted to python code internally), but has a slightly simpler syntax.
For the Python fans: no worries, there is still Python under the hood and you can use all packages of your python environment also here. For the Python beginnners: you do not necessarily have to learn Python in order to write attune code, but should be familiar with the basics of programming as well as object-oriented programming. Vice versa, you do not necessarily have to learn the attune language in order to use sfctools. It is just an additional benefit for you! The first rule is: except function declarations and some additional magic, there is no major difference to Python. Whenever there is no deviating syntax, just use what you know from Python! Second rule: Quite often, agents refer to themselves. Therefore, we have abbreviated 'self.' with '$.'

All other things you have to know are explained below.

Agents
---------------------
Agents are created like follows:

.. code-block:: python

    $[AGENT] name_of_agent
       < your code here >
    $[ENDAGENT]


Agents can refer to themselfes (i.e. to their own internal storage) via "$.some_value" (dollar-dot) or just the Python way, i.e. with "self".
Function calls ('methods', see below) are executed with an open and closing bracket ("$.function()"), whereas parameters ('attributes') are just accessed via "$.my_value".


Init sequence
---------------------
The init block is called whenever a new instance of this agent type is created.

.. code-block:: python

    $[AGENT] name_of_agent
       < your code here >

    +[INIT]
    $.a = 5
    $.b = 6
    $.c = 'hello world'
    +[ENDINIT]

    $[ENDAGENT]


Read the settings
---------------------
To avoid hard-coding, we can read parameters from the settings yaml file. All we have to do is list them in the [PARAM] block

.. code-block:: python


    $[AGENT] name_of_agent

    +[INIT]
    ...
    +[ENDINIT]

    +[PARAM] alpha, beta, gamma

    $[ENDAGENT]

This will cause a search in sfctools "Settings()" and return the corresponding value.


Links between agents
---------------------
Agents can be made visible to other agents. To do so, specify a [KNOWS] block like this:

.. code-block:: python


    $[AGENT] name_of_agent

    +[INIT]
    ...
    +[ENDINIT]

    +[PARAM] alpha, beta, gamma

    +[KNOWS] agent_type, another_agent_type


    +[FUN] print_friends
    print("I have %i friends" % (len(@agent_type) + len(@another_agent_type)))
    for agent in @agent_type:
        print("my friend", agent)
    for agent in @another_agent_type:
        print("my other friend", agent)
    +[ENDFUN]

    $[ENDAGENT]

Functions (Methods)
---------------------
Agents can execute some internal updating rules. The default updating rules have no parameters.
In this case, simply write

.. code-block:: python

    +[FUN] my_function
    <your code here >
    +[ENDFUN]


If you wish to have input and output parameters, you can specify them just as in Python and also return them like in Python:


.. code-block:: python

    +[FUN] my_function (a,b)
    <your code here >
    val = a+b
    return val
    +[ENDFUN]

there are no spaces allowed in the parameter definition (first line of [FUN]). spaces will corrupt your code!

Custom Imports
---------------------
Numpy and some basic sfctools packages are automatically imported.
However, if you wish to import an additional module, you can do so by specificying an import block

.. code-block:: python

    +[IMPORT] import matplotlib.pyplot as plt
    +[IMPORT] import pandas as pd
    ...

    $[AGENT] name_of_agent
       < your code here >
    $[ENDAGENT]


Transactions
---------------------
In Attune, transactions can be highlighted by simply writing "<~~>" in front of the transaction specification
Example:

.. code-block:: python

    $[AGENT] name_of_agent

    ...

    +[KNOWS] Glurak

    +[FUN] some_weird_function (a)
    other = @Glurak[0]
    <~~> some_transaction(self, other, 2*a)
    +[ENDFUN]

    ...
    $[ENDAGENT]


Balance Sheet Operations
--------------------------
Balance sheet operations can be formulated in a simplfied syntax. Allowed entry types are EQUITY, ASSETS, LIABILITIES

.. code-block:: python

    +[ACCOUNTING]
    <> "Cash", ASSETS, 10.0
    <> "Equity", EQUITY, 10.0
    +[ACCOUNTING]

silent balance sheet operations can be written as "<.>" and will not be part of the flow matrix.

.. code-block:: python

    +[ACCOUNTING]
    <.> "Cash", ASSETS, 10.0
    <.> "Equity", EQUITY, 10.0
    +[ACCOUNTING]

Further example

.. code-block:: python

    +[ACCOUNTING]
    <.> "Cash", ASSETS, 1.0
    <.> "Debt", LIABILITIES, 1.0
    +[ACCOUNTING]

You can get the balance via 'BALANCE?'

.. code-block:: python

    +[FUN] get_my_balance
    x = BALANCE?("Cash",ASSETS)
    return x
    +[ENDFUN]


Income Statement
---------------------
The income statement can be accessed via '$.income_statement'. Everything else is specified in the sfctools documentation.

.. code-block:: python

    +[FUN] get_my_taxes
    t = $.income_statement.gross_income - $.income_statement.net_income
    return t
    +[ENDFUN]
