
The Modeling Workflow
-----------------------
*sfctools* was designed to be used by both engineering-oriented and economics-oriented scholars who have basic education in both fields. It can be used by a single developer or by a small development team, splitting the work of model creation in terms of consistency and economic logic from the actual programming and technical implementation. This allows software solutions from rapid prototyping up to sophisticated, small-to-medium-sized ABMs. It is therefore a versatile virtual laboratory for agent-based economics.
In particular, we distinguish between three roles: i) the model designer, who conceptualizes the main idea of a model and accompanies the project ideally throughout the whole lifetime of a project, ii) the software developer who ensures the correct software implementation of the model and is responsible for the technical realization, iii) the model user who customizes the core model to a specific use case, parametrization, scenario or particular research question. These three roles can, of course, be occupied by one and the same person, or be separated among a small research team.

.. image:: workflow.png
    :width: 1050px
    :alt: alternate text

At the beginning of each project, the modeler has to decide wether she/he wants to use the graphical interface *sfctools-attune* or wants to create a standalone Python project. When working with attune, the user can fully describe the agents in the realtively simple (but fully pythonic!) agent parametrization language (code is placed inside the *./mamba_code* directory). When working in a standalone project, the *sfctools* framework (and all its particular features) can be imported as a package directly in the code. This allows for operation modes involving different team members and their individual skillset. 


The next page will deal with the first approach, i.e. working with *sfctools-attune*.