Getting Started
============================
sfctools-Attune is an AgenT-based Transaction and accoUNting graphical user interfacE.

In this simple graphical interface, transactions can be edited, sorted and graphically analyzed. Also, strucutral parameters are edited in form of a yaml-styled summary. The GUI allows for several development productivity tools, such as the analytical pre-construction of the transaction flow matrix.The main window of the GUI contains of three sub-panels: First, it shows the transaction panel (top left, mid left panels). Here, the user can directly access the balance sheets of the 'sender' and the 'receiver' agent, which are both equipped with a double entry balance sheet system. Also, the entries for income and cash flow can be manually set in this transaction, and the user can define which flows and stocks are addressed. Second, the user can access already created transactions within the transaction view (panel on the bottom left). Here, transactions can be edited, deleted or sorted. All details for transactions are given here as an overview. Third, a graphical representation of the agents is generated in the graph view (panel on the right), where different flows are visualized and can be filtered by balance sheet items or by involved agents in an interactive way. The user can freely layout and colorize the created agent relations in a graph structure.
In this paragraph, you will get familiar with the main panels of the GUI interface and their respective functionality.


Starting the GUI
----------------------
The GUI is part of the python package sfctools and can be simply called via

.. code-block::

   python -m sfctools attune

the GUI requires the graphics toolkit PyQt5, which is automatically installed with sfctools.


The Transaction Editor
------------------------
The transaction editor is the heart of the interface. It allows the user to specify the exact contents of each transaction to be deployed to the final model. Each transaction (which can be edited here) consist of exactly two participating agent types (self-reference is also allowed, but no third agent is involved). On the left and right half, the double balance sheet systems of the two parties are shown. On the left, changes in assets are described. On the right, changes in liabilities and equity are described. These changes are formulated using a single line for each transaction containing of the sign ('+'=debit, '-'=credit) and the name of the item to be changed (e.g. 'Deposits' or 'Loans'). In the text fields below, further specifications have to be filled out. The corresponding explanations are as follows:


- **SENDER** / **RECEIVER**: Enter the exact name of the agents here (first letter uppercase, then lowercase. )

- **Name**:  The name of the transcation. This will be also the name of the Python function stored in the transactions.py file later. Therefore, a lowercase name is recommended for consistency reasons. The name will also appear as the label of the transaction in the graph view.
- **Subject**: This will label the line of the transaction flow matrix, i.e. is meant to be a representative description for a number of individual transactions (e.g. 'taxes' or 'interest'). Here, also lowercase is recommended
- **Quantity**: This should be a short analytical expression for the quantity, e.g. 'C' for consumption or 'T_f' for firms' taxes. This symbol will appear in the analytical transaction flow matrix.
- **Description**: Put a meaningful but short description of this transaction for documentation purposes here.

The 'Update Values' button can be used if the current values correspond to an already existing transaction and will overwrite the old values with the newly entered ones (WARNING: this cannot be undone).

The right panel (reigster flow, tpye, unidirectional)  and the bottom panel (income sender/receiver, cashflow sender/receiver) are optional fields and are explained below:

- *Register Flow* This option can be True or False. It indicates if a transaction appears on the upper half of the transaction flow matrix, i.e. if it is registered as an actual flow. Some transactions might only affect changes in stocks, but not an effective flow. Make sure not to use this function correclty in order to avoid the mis-specification of certain types of transactions in the transaction flow matrix.

- *KA->CA ... * This field sets the accounts involved in the corresponding transaction. KA stands for capital account, CA stands for current account.

- *Unidirectional* This can be True or False. Setting 'unidirectional' to True will allow for negative quantities to be passed in the numerical part of the simulation. This can be useful for some transactions. If 'False' is set, an automatic check will be run to ensure transaction quantities of greater or equal to zero.

- *Income Statement* This field creates an income statement entry whenever the corresponding transaction is activated (called). There are various fields available, which can be in detail viewed in the API reference. Choosing 'None' deactivates this field. Signs are automatically determined.

- *Cash Flow Statement* This field creates an cash flow statement entry whenever the corresponding transaction is activated (called). The cash flow statment should only be altered when the cash flow of an agent is actually affected.  There are various fields available, which can be in detail viewed in the API reference. Choosing 'None' deactivates this field. Signs are automatically determined.


The Transaction View
---------------------
The transaction view is at the bottom left of the main window of attune. You can see the following entries: subject, agent_from, agent_to, short_name, flow, unidirectional, quantity. The entries can be manually sorted using the 'move up' and 'move down' buttons.  The 'Add New' button adds any transaction entered in the transaction editor panel. The 'Remove' button removes the currently selected transaction (WARNING: this cannot be undone).


The Graph View
---------------------
The graph view will show a graphical representation of the model. Herein, the large boxes represent different types of agents. The small circles ('handles') can be dragged for better visual representation and are selectors for the transactions (indicated by lines connecting the boxes). The mouse wheel can be used to zoom in/out. On the bottom left, a small preview of the full model is shown.

- **Show income**:  shows the total income statement summary for a selected agent
- **Show cashflow**: shows the total cashflow summary for a selected agent
- **re-order**: reorders the connection lines between the boxes randomly (WARNING: this cannot be undone)
- **COLORS**: this panel allows the users to fully customize their color scheme. This works in bright as well as in dark mode. When saving a project, the theme will also be saved alongside. In addition the 'save/load' buttons allow for custom exports or imports of customized color themes for better visualization of the model. The 'restore' button restores all colors to default (WARNING: this cannot be undone)
- **Filter** this is a filter for a certain balance sheet item. If any are selected, only the transactions (connections) are shown which affect the corresponding balance sheet items. The 'clear' button allows to clear the filter (WARNING: this cannot be undone)
- **Show...** these checkboxes are here for a customized user experience.

Creating an Agent
---------------------

To create an agent, insert the name (first letter uppercase) of the agent type in the text field of the graph view and click the button 'Add'. A new box with the agent type will appear in the graph view. Edit the agent's source code by double-clicking on the box or by selecting the corresponding box and clicking the 'edit agent' button.
An alternative way to create an agent is by adding a new transaction. If the agent type does not exist yet, *sfctools-attune* will ask you to create one for you in a dialog window.

Settings and Parameters
------------------------

The settings and parameters window can be accessed via Settings -> Edit Settings. The settings window is divided into two panels. On the left panel, the settings.yml is displayed in raw format. On the right panel, a graphically more appealing summary of the data is shown. The top right label ('valid'/ 'invalid') shows if the entered text corresponds to a valid yaml format and is a valid settings string. On the bottom right (search), users can search for specific parameters.

- **Metainfo**:  This section provides a rough set of metadata about the author, the creation date and short summary ('info') about the project.

- **Hyperparams**: This section consists of a list of 3-tuples of parameters. Each parameter is defined as follows

.. code-block::

       - name: example
         value: 42.0
         description: an example parameter


where 'name' is the name of the parameter. The parameter can later be accessed via the 'Settings()' object of sfctools, or via the '+[PARAM]' line in the code. The 'value' field stores the actual value of each parameter, the 'description' field can be used for a short description of the parameters. Beware that the parameters can, in principle, be altered (overwritten) throughout the simulation for single agents, but should rather be consistently used as fixed structural parameters ('hyperparameters') for a model. The settings are automatically saved.
