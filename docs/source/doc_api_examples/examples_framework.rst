API Examples
==============

This is a collection of examples for the framework API. The example folder can also be found on gitlab: https://gitlab.com/dlr-ve/esy/sfctools/framework/-/tree/main/sfctools/examples

.. toctree::
    :maxdepth: 1

    basic_example.rst
    balance.rst
    flowmatrix.rst
    market.rst
    clock.rst
    monte_carlo.rst
    worker_registry.rst
    bank_order_book.rst
    inventory.rst
    signals_slots.rst
