Inventory
===============

Simple inventory structure example

Code
-----

.. code-block:: python

    from sfctools.datastructs.inventory import Inventory
    from sfctools import Agent

    class InventoryAgent(Agent): # basic agent with an inventory
        def __init__(self):
            super().__init__()
            self.inventory = Inventory(self)
    
    # perform some arbitrary manipulations on the inventory
    my_agent = InventoryAgent()
    my_agent.inventory.add_item("Apple",1,3.0)
    my_agent.inventory.add_item("Apple",1,5.0)
    my_agent.inventory.remove_item("Apple", 1)
    w = my_agent.inventory.worth
    my_agent.inventory.add_item("Pear",2,2.0)
    my_agent.inventory.remove_item("Pear", 2)

    n_a = my_agent.inventory.get_inventory("Apple")
    n_p = my_agent.inventory.get_inventory("Pear")
    w2 = my_agent.inventory.worth

    print("n_a",n_a)
    print("n_p",n_p)
    print("w2",w2)


Output
--------------

.. code-block:: text

    n_a 1.0
    n_p 0.0
    w2 4.0
