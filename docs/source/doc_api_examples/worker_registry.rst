Worker Registry
=================

Simple worker registry data structure for ABM firm agents

Code
---------

.. code-block:: python

    from sfctools.datastructs.worker_registry import WorkerRegistry
    from sfctools import Agent
    import numpy as np
    np.random.seed(1980)

    class Worker(Agent):
        def __init__(self):
            super().__init__()
            self.reservation_wage = np.random.rand()

    a = Agent()

    wreg = WorkerRegistry(owner=a,wage_attr="reservation_wage") # create a new worker registry

    workers = [Worker() for i in range(10)] # create a bunch of workers

    for worker in workers: # add workers to registry
        wreg.add_worker(worker)

    costs = wreg.get_avg_costs() # get average reservation wage of all workers
    print("Average cost: %.2f\n" % costs)

    wreg.fire_random(2) # fire two random workers

    # pop workers from the worker stack
    print("Wokers left:")
    for i in range(8):
        print(wreg.pop())

Output
--------------

.. code-block:: text

    Average cost: 0.55

    Wokers left:
    Worker__00009
    Worker__00008
    Worker__00007
    Worker__00006
    Worker__00005
    Worker__00003
    Worker__00002
    Worker__00001
