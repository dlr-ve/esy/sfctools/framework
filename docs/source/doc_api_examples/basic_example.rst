Basic Example
=====================

The framework can be included in any python project. Agents are constructed by inheriting from the Agent class:

Code
---------

.. code-block:: python

  from sfctools import Settings, Agent, FlowMatrix, BalanceEntry
  from sfctools import Accounts

  Settings().read_from_yaml("my_settings.yml") # <- defines parameter 'beta'

  class MyAgent(Agent):
    def __init__(self):
      super().__init__()

      self.my_parameter = Settings().get_hyperparameter("beta")

    # def more_here(self,*args):
    #  ...

  print(Settings())

  my_agent = MyAgent() # <- create an agent
  my_second_agent = MyAgent()  # <- create a second agent

  with my_agent.balance_sheet.modify:
    my_agent.balance_sheet.change_item("Cash", BalanceEntry.ASSETS, 10.0)
    my_agent.balance_sheet.change_item("Equity", BalanceEntry.EQUITY, 10.0)  # enlarge my_agent's balance by 10

  def my_test_transaction(agent1,agent2,quantity):
    FlowMatrix().log_flow((Accounts.CA, Accounts.CA), quantity, agent1,agent2,subject="test")

  my_test_transaction(my_agent,my_second_agent,9.0) # transfer 9 units between the agents

  print(my_agent.balance_sheet.to_string())
  print(my_second_agent.balance_sheet.to_string())

  print(FlowMatrix().to_string(group=False))

And the my_settings.yml:

.. code-block:: yaml

            metainfo :
                author: Example Author
                date: November 2021
                info: settings for example project

            hyperparams:
                - name: beta
                  value: 0.05
                  description: just an example parameter


Output
-----------

Settings

.. code-block:: text


        Value                Description
  Name
  beta   0.05  just an example parameter


Balance sheet of first agent

.. code-block:: text


                                    Assets    Equity Liabilities
  BALANCE SHEET OF MyAgent__00001
  Cash                                 10.0     .-          .-
  Equity                              .-         10.0       .-
  Total                                10.0      10.0         0.0


Balance sheet of second agent

.. code-block:: text


                                  Assets  Equity  Liabilities
  BALANCE SHEET OF MyAgent__00002
  Total                               0.0     0.0          0.0


FlowMatrix printout


.. code-block:: text

        MyAgent__00001       MyAgent__00002 Total
                    CA    KA             CA
  test             -9.0   0.0            9.0   0.0
  Δ Cash            0.0 -10.0            0.0 -10.0
  Total            -9.0 -10.0            9.0 -10.0
