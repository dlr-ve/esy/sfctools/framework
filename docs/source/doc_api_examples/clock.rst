Clock
==============

Code
---------

Clock example for a clock with yearly time interval

.. code-block:: python

    from sfctools import Clock
    import datetime
    from dateutil.relativedelta import relativedelta

    t0 = datetime.datetime(2020, 1, 1) # set up initial time

    clock = Clock(t0=t0, dt=relativedelta(years=1)) # setup a clock
    t0 = clock.get_time() # get the starting time

    [clock.tick() for i in range(5)] # tick 5 times

    t1 = clock.get_time()        # get the integer time
    t2 = clock.get_real_time()   # get the actual datetime time

    clock.reset() # reset clock to zero

    t3 = clock.get_time()
    t4 = clock.get_real_time()

    print("t0:",t0)
    print("t1:",t1)
    print("t2:",t2)
    print("t3:",t3)
    print("t4:",t4)

Output
---------

.. code-block:: text

    t0: 0
    t1: 5
    t2: 2025-01-01 00:00:00
    t3: 0
    t4: 2020-01-01 00:00:00
