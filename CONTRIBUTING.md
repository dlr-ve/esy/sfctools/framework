# Sfctools Description for Contributors
- Author: Thomas Baldauf
- Contact: thomas.baldauf@dlr.de
- Version: 0.4-chicha-beta

Contributing to sfctools
===========================

Project Description
----------------------

Sfctools is a lightweight and easy-to-use Python framework for agent-based macroeconomic, stock-flow consistent (ABM-SFC) modeling. Unlike more generic frameworks like mesa, it concentrates on agents in economics. It helps you to construct agents, helps you to manage and document your model parameters, assures stock-flow consistency, and facilitates basic economic data structures (such as the balance sheet).

Contribute
----------------------

Sfctools is open for your contribution. If you are interested in contributing to the code or the documentation, you are greatfully invited to do so! If you want to contribute to this repository, please discuss the changes you wish to make, or new features you want to introduce, with the core development team. Please also try to adapt your code style in accordance with the coding conventions and quality values of this repository. Read the Contributors License Agreement (CLA) and send a signed copy to Thomas Baldauf (thomas.baldauf@dlr.de), German Aerospace Center, Institute of Networked Energy Systems Stuttgart.


More information regarding code structure, design principles and architecture choices can be found in the sfctools documentation. If you wish to contribute, feel free to get in touch directly or open an issue on the repository. We currently don't support automatic pushes.

The project maintains the following source code repositories

    https://gitlab.com/dlr-ve/esy/sfctools
